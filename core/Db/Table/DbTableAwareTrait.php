<?php
namespace Core\Db\Table;


trait DbTableAwareTrait {

    /**
     * @var Table
     */
    protected $dbTable;

    /**
     * @param Table $table
     * @return $this
     */
    public function setDbTable(Table $table)
    {
        $this -> dbTable = $table;

        if (method_exists($this, 'dbTableInit')) {
            $this -> dbTableInit();
        }

        return $this;
    } // setDbTable()


    /**
     * @return Table
     * @throws Exception\DbTableUndefinedException
     */
    public function getDbTable()
    {
        if (null === $this -> dbTable) {
            throw new Exception\DbTableUndefinedException('DbTable is undefined');
        }
        return $this -> dbTable;
    } // getDbTable()

} 