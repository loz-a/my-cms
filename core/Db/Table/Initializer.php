<?php
namespace Core\Db\Table;

use Zend\Filter\Word\CamelCaseToUnderscore;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;

class Initializer implements InitializerInterface
{
    /**
     * @var \PDO
     */
    protected $dbConnection;

    /**
     * @var \Zend\Config\Config
     */
    protected $tablesNames;

    /**
     * @var CamelCaseToUnderscore
     */
    protected $camelCaseToUnderscoreFilterInstance;

    /**
     * Initialize
     *
     * @param $instance
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        $tableAlias = $this -> getTableAlias($instance);


        if ($instance instanceof HydratorAwareInterface) {
            $hydratorAlias = sprintf('%s.hydrator', $tableAlias);
            $hydrator      = $serviceLocator -> has($hydratorAlias) ? $serviceLocator -> get($hydratorAlias) : new ClassMethods();
            $instance -> setHydrator($hydrator);
        }

        if ($instance instanceof DbTableAwareInterface) {

            if (null === $this -> dbConnection) {
                $this -> dbConnection = $serviceLocator -> get('db.connection');
            }

            $dbTable = (new Table()) -> setConnection($this -> dbConnection);

            if (null === $this -> tablesNames) {
                $this -> tablesNames = $serviceLocator -> get('config') -> get('db') -> get('tables_names');
            }

            $tables = $this -> tablesNames -> get($tableAlias);

            if (null !== $tables) {
                if (is_string($tables)) {
                    $dbTable -> setTableName($tables);
                }
                else {
                    $tables = $tables -> toArray();

                    if (isset($tables[$tableAlias])) {
                        $dbTable -> setTableName($tables[$tableAlias]);
                        unset($tables[$tableAlias]);
                    }
                    $dbTable -> setRelatedTablesNames($tables);
                }
            }

            $instance -> setDbTable($dbTable);
        }

    } // initialize()


    /**
     * @return CamelCaseToUnderscore
     */
    protected function getCamelCaseToUderscoreFilter()
    {
        if (null === $this -> camelCaseToUnderscoreFilterInstance) {
            $this -> camelCaseToUnderscoreFilterInstance = new CamelCaseToUnderscore();
        }

        return $this -> camelCaseToUnderscoreFilterInstance;
    } // getCamelCaseToUderscoreFilter()


    /**
     * @param $instance
     * @return string
     */
    protected function getTableAlias($instance)
    {
        $exploded = explode('\\', get_class($instance));
        $tableAlias = $this -> getCamelCaseToUderscoreFilter() -> filter(end($exploded));
        return strtolower($tableAlias);
    } // getTableAlias()
} 