<?php
namespace Core\Db\Table\Exception;


class InvalidRelatedTableNameException extends \Exception
    implements ExceptionInterface
{

} 