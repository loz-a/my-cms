<?php
namespace Core\Db\Table\Exception;

class DbTableUndefinedException extends \Exception
    implements ExceptionInterface
{

} 