<?php
namespace Core\Db\Table;

interface DbTableAwareInterface
{
    /**
     * @param Table $table
     * @return mixed
     */
    public function setDbTable(Table $table);

    /**
     * @return Table
     */
    public function getDbTable();
} 