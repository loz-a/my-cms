<?php
namespace Core\Db\Table;

use Core\Db\Sql\FindBuilder;
use Core\Db\Sql\QueryBuilder;
use PDO;

class Table
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string|array
     */
    protected $primaryKey = array('id');

    /**
     * @var PDO
     */
    protected $connection;

    /**
     * @var array
     */
    protected $relatedTablesNames = array();


    /**
     * @var FindBuilder
     */
    protected $findBuilder;

    /**
     * @param PDO $connection
     * @return $this
     */
    public function setConnection(PDO $connection)
    {
        $this -> connection = $connection;
        return $this;
    } // setConnection()


    /**
     * @return PDO
     * @throws Exception\InvalidConnectionException
     */
    public function getConnection()
    {
        if (null === $this -> connection) {
            throw new Exception\InvalidConnectionException('Connection is undefined');
        }
        return $this -> connection;
    } // getAdapter()


    /**
     * @param string $tableName
     * @return $this
     */
    public function setTableName($tableName)
    {
        $this -> tableName = (string) $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        if (null === $this -> tableName) {
            $className = explode('\\', get_class($this));
            $this -> tableName = strtolower($className[count($className) - 1]);
        }
        return $this -> tableName;
    } // getTableName()


    /**
     * @param array $relatedTablesNames
     * @return $this
     */
    public function setRelatedTablesNames(array $relatedTablesNames)
    {
        $this -> relatedTablesNames = $relatedTablesNames;
        return $this;
    } // setRelatedTablesNames()


    /**
     * @param $alias
     * @return string
     * @throws Exception\InvalidRelatedTableNameException
     */
    public function getRelatedTableName($alias)
    {
        if (array_key_exists($alias, $this -> relatedTablesNames)) {
            return $this -> relatedTablesNames[$alias];
        }
        throw new Exception\InvalidRelatedTableNameException('Table name is invalid');
    } // getRelatedTableName()


    /**
     * @return FindBuilder
     */
    public function getFindBuilder()
    {
        if (null === $this -> findBuilder) {
            $this -> findBuilder = (new FindBuilder()) -> setTableName($this -> getTableName());
        }
        return $this -> findBuilder;
    } // getFindBuilder()


    /**
     * @param string|array $primaryKey
     * @return $this
     */
    public function setPrimaryKey($primaryKey)
    {
        $this -> primaryKey = is_array($primaryKey) ? $primaryKey : func_get_args();
        return $this;
    } // setPrimaryKey()


    /**
     * @return array|string
     */
    public function getPrimaryKey()
    {
        return $this -> primaryKey;
    } // getPrimaryKey()


    /**
     * @param $sql
     * @param array $boundParams
     * @return \Generator
     */
    public function fetch($sql, array $boundParams = null)
    {
        $sth = $this -> executeSql($sql, $boundParams);

        while ($row = $sth -> fetch(PDO::FETCH_ASSOC)) {
            yield $row;
        }
    } // fetch()


    /**
     * @param $sql
     * @param array $boundParams
     * @return array
     */
    public function fetchAll($sql = null, array $boundParams = null)
    {
        if (null === $sql) {
            $sql = new QueryBuilder();
        }
        return $this -> executeSql($sql, $boundParams) -> fetchAll(PDO::FETCH_ASSOC);
    } // fetchAll()


    /**
     * @param $sql
     * @param array $boundParams
     * @return mixed
     */
    public function fetchOne($sql, array $boundParams = null)
    {
        return $this -> executeSql($sql, $boundParams) -> fetch(PDO::FETCH_ASSOC);
    } // fetchAll()


    /**
     * @param $sql
     * @param array $boundParams
     * @param $columnNumber
     * @return mixed
     */
    public function fetchColumn($sql, array $boundParams = null, $columnNumber = 0)
    {
        return $this -> executeSql($sql, $boundParams) -> fetchColumn($columnNumber);
    } // fetchColumn()


    /**
     * @param $values
     * @return array|\Generator
     * @throws Exception\InvalidArgumentException
     */
    public function getByPk($values)
    {
        if (!is_array($values)) {
            $values = func_get_args();
        }
        $func = sprintf('findOneBy%s', ucfirst($this -> primaryKey[0]));

        for ($i = 1; $i < sizeof($this -> primaryKey); $i++) {
            $pk = $this -> primaryKey[$i];

            if (!isset($values[$i])) {
                throw new Exception\InvalidArgumentException(sprintf('No value for field %s', $pk));
            }
            $func = sprintf('%sAnd%s', $func, ucfirst($pk));
        }
        return $this -> __call($func, $values);
    } // getByPk()


    /**
     * @param array $data
     * @param string $tableName
     * @return \PDOStatement
     */
    public function insert(array $data, $tableName = '')
    {
        return $this -> executeSql(
            (new QueryBuilder\Insert()) -> columns(array_keys($data)) -> values(array_values($data)),
            null,
            $tableName
        );
    } // insert()


    /**
     * @param array $data
     * @param $id
     * @param string $tableName
     * @return \PDOStatement
     */
    public function update(array $data, $id, $tableName = '')
    {
        return $this -> executeSql(
            (new QueryBuilder\Update()) -> set($data) -> where() -> equal('id', $id),
            null,
            $tableName
        );
    } // update()


    /**
     * @param $id
     * @param string $tableName
     * @return int
     */
    public function delete($id, $tableName = '')
    {
        $sth = $this -> executeSql(
            (new QueryBuilder\Delete()) -> where() -> equal('id', $id),
            null,
            $tableName
        );

        return $sth -> rowCount();
    } // delete()


    /**
     * @param $sql
     * @param array $boundParams
     * @param string $tableName
     * @return \PDOStatement
     */
    public function executeSql( $sql, array $boundParams = null, $tableName = '')
    {
        if ($sql instanceof QueryBuilder\BuilderInterface) {

            if (method_exists($sql, 'qb')) {
                $sql = $sql -> qb();
            }

            if ($sql instanceof QueryBuilder\From\CheckTableNameInterface
                and !$sql -> hasTableName()
            ){
                $table = $tableName ? strval($tableName) : $this -> getTableName();

                switch(true) {
                    case $sql instanceof QueryBuilder:
                    case $sql instanceof QueryBuilder\Delete:
                        $sql -> from($table);
                        break;
                    case $sql instanceof QueryBuilder\Insert:
                        $sql -> into($table);
                        break;
                    case $sql instanceof QueryBuilder\Update:
                        $sql -> table($table);
                        break;
                }
            }
            list($sql, $boundParams) = $sql -> build();
        }

        $sth = $this -> getConnection() -> prepare($sql);
        $sth -> execute($boundParams);

        return $sth;
    } // executeSql()


    /**
     * @param $name
     * @param $args
     * @return \Generator|array
     * @throws Exception\BadMethodCallException
     */
    public function __call($name, $args)
    {
        $sql = call_user_func_array(array($this -> getFindBuilder(), $name), $args);

        if (null === $sql) {
            throw new Exception\BadMethodCallException(sprintf('Invalid method - %s', $name));
        }

        list($sql, $boundParams) = $sql;
        if ($this -> hasLimitOne($sql)) {
            return $this -> fetchOne($sql, $boundParams);
        }
        return $this -> fetch($sql, $boundParams);
    } // __call()


    /**
     * @param $sqlString
     * @return int
     */
    public function hasLimitOne($sqlString)
    {
        return strpos($sqlString, 'limit 1', strlen($sqlString) - strlen('limit 1'));
    } // hasLimitOne()

} 