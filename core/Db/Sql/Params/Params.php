<?php
namespace Core\Db\Sql\Params;

use Core\Stdlib\Params as BaseParams;

class Params extends BaseParams
{
    /**
     * @var int
     */
    protected static $varIndex = 0;

    /**
     * @var string
     */
    protected $keyPrefix;


    public static function resetVarIndex()
    {
        self::$varIndex = 0;
    } // resetVarIndex()


    /**
     * @param $prefix
     * @return $this
     */
    public function setKeyPrefix($prefix)
    {
        $this -> keyPrefix = $prefix;
        return $this;
    } // setKeyPrefix()


    /**
     * @return string
     */
    public function getKeyPrefix()
    {
        return $this -> keyPrefix;
    } // getKeyPrefix()


    /**
     * @param $key
     * @return string
     */
    public function key($key = '')
    {
        if (strpos($key, ':') !== 0) {
            $keyPrefix = $this -> getKeyPrefix();
            $counter   = self::$varIndex++;
            $key = ('' === $key) ? sprintf(':%s_%d', $keyPrefix, $counter) : sprintf(':%s_%s_%d', $keyPrefix, $key, $counter);
        }
        return strtolower($key);
    } // key()


    /**
     * @param array|string $key
     * @param null $value
     * @return $this
     */
    public function set($key, $value = null)
    {
        if (is_array($key)) {
            $result = array();
            foreach ($key as $k => $val) {
                $result[$this -> key($k)] = $val;
            }
            return parent::set($result, null);
        }
        return parent::set($this -> key($key), $value);
    } // set()

} 