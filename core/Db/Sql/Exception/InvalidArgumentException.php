<?php
namespace Core\Db\Sql\Exception;

class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

} 