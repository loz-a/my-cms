<?php
namespace Core\Db\Sql;

use Core\Db\Sql\QueryBuilder\BoundParamsTrait;
use Core\Db\Sql\QueryBuilder\BuilderInterface;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameInterface;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameTrait;

class QueryBuilder implements BuilderInterface, CheckTableNameInterface
{
    use BoundParamsTrait, CheckTableNameTrait;

    /**
     * @var QueryBuilder\Select
     */
    protected $select;

    /**
     * @var string|SubqueryBuilder
     */
    protected $table;

    /**
     * @var QueryBuilder\Where
     */
    protected $where;

    /**
     * @var QueryBuilder\Join
     */
    protected $join = array();

    /**
     * @var QueryBuilder\Group
     */
    protected $group;

    /**
     * @var QueryBuilder\Order
     */
    protected $order;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @return $this
     */
    public function reset()
    {
        $this -> select = null;
        $this -> table  = null;
        $this -> where  = null;
        $this -> join   = array();
        $this -> group  = null;
        $this -> order  = null;
        $this -> limit  = null;
        return $this;
    } // reset()


    /**
     * @param null $columns
     * @return QueryBuilder\Select
     */
    public function select($columns = null)
    {
        if (null === $this -> select) {
            $this -> select = new QueryBuilder\Select($this);
        }

        if (is_array($columns)) {
            $this -> select -> columns($columns);
        } else if (func_num_args() > 0) {
            $this -> select -> columns(func_get_args());
        }

        return $this -> select;
    } // select()


    /**
     * @param $table
     * @param null $alias
     * @return $this
     */
    public function from($table, $alias = null)
    {
        if ($table instanceof BuilderInterface) {
            if (method_exists($table, 'qb')) {
                $table = $table -> qb();
            }

            list($table, $boundParams) = $table -> build();
            $this -> params() -> set($boundParams);
        }
        $this -> table = (null !== $alias) ? sprintf('from %s as %s', $table, $alias) : sprintf('from %s', $table);

        return $this;
    } // from()


    /**
     * @return QueryBuilder\Where
     */
    public function where($identifier = null, $value = null)
    {
        if (null === $this -> where) {
            $this -> where = new QueryBuilder\Where($this);
        }

        if ($identifier and $value) {
            $this -> where -> equal($identifier, $value);
        }

        return $this -> where;
    } // where()


    /**
     * @param null $tableName
     * @param null $alias
     * @return QueryBuilder\Join
     */
    public function join($tableName = null, $alias = null)
    {
        $join = new QueryBuilder\Join($this);

        if (null !== $tableName) {
            $join -> with($tableName, $alias);
        }
        $this -> join[] = $join;
        return $join;
    } // join()


    /**
     * @param null $columns
     * @return QueryBuilder\Group
     */
    public function group($columns = null)
    {
        if (null === $this -> group) {
            $this -> group = new QueryBuilder\Group($this);
        }

        if (is_array($columns)) {
            $this -> group -> by($columns);
        }
        else if (func_num_args() > 0) {
            $this -> group -> by(func_get_args());
        }

        return $this -> group;
    } // group()


    /**
     * @return QueryBuilder\Order
     */
    public function order()
    {
        if (null === $this -> order) {
            $this -> order = new QueryBuilder\Order($this);
        }
        return $this -> order;
    } // order()


    /**
     * @param $limit
     * @param null $offset
     * @return $this
     */
    public function limit($limit, $offset = null)
    {
        $limit = (int) $limit;

        if ($offset) {
            $offset = (int) $offset;
            $this -> limit = sprintf('limit %s, %s', $offset, $limit);
        }
        else {
            $this -> limit =  sprintf('limit %s', $limit);
        }
        return $this;
    } // limit()


    /**
     * @param $name
     * @return null|string
     * @throws QueryBuilder\Exception\InvalidClauseException
     */
    protected function buildStatement($name)
    {
        if (!property_exists($this, $name)) {
            throw new QueryBuilder\Exception\InvalidClauseException(sprintf('Unknown statement %s', $name));
        }
        $stmt = $this -> $name;

        if ($stmt instanceof BuilderInterface) {

            if (method_exists($this -> $name, 'params')) {
                $this -> params() -> set($this -> $name -> params() -> toArray());
            }
            return $stmt -> build();
        }
        else if (is_array($stmt) and count($stmt)) {
            return $this -> buildStatementFromArray($stmt);
        }
        else if (is_string($stmt)) {
            return $stmt;
        }

        return null;
    } // buildStatement()


    /**
     * @param $statements
     * @return string
     */
    protected function buildStatementFromArray($statements)
    {
        $result = array();
        for($i = 0, $l = count($statements); $i < $l; $i++) {

            if ($statements[$i] instanceof BuilderInterface) {

                if (method_exists($statements[$i], 'params')) {
                    $this -> params() -> set($statements[$i] -> params() -> toArray());
                }
                $result[] = $statements[$i] -> build();
            }
            else if (is_string($statements[$i])) {
                $result[] = $statements[$i];
            }
        }
        return trim(implode(' ', $result));
    } // buildStatementFromArray()


    /**
     * @return array
     */
    public function build()
    {
        $statementsNames = array('select', 'table', 'join', 'where', 'group', 'order', 'limit');
        $temp = array();

        if (null === $this -> select) {
            $this -> select();
        }

        foreach ($statementsNames as $name) {
            $stmt = $this -> buildStatement($name);
            if (is_string($stmt)) {
                $temp[] = $stmt;
            }
        }

        return array(trim(implode(' ', $temp)),$this -> params() -> toArray());
    }
}