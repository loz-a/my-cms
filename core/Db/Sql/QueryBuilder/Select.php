<?php
namespace Core\Db\Sql\QueryBuilder;


class Select implements BuilderInterface
{
    use QueryBuilderAwareTrait;

    /**
     * @var array|string
     */
    protected $columns;

    /**
     * @var bool
     */
    protected $isDistinct = false;

    /**
     * @var array
     */
    protected $functions;

    /**
     * @param string $columns
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function columns($columns = '*')
    {
        $this -> columns = is_array($columns) ? $columns : func_get_args();
        return $this;
    } // columns()


    /**
     * @param bool $isDistinct
     * @return $this
     */
    public function distinct($isDistinct = true)
    {
        $this -> isDistinct = (bool) $isDistinct;
        return $this;
    } // distinct()


    public function count($column = '', $alias = null, $isDistinct = false)
    {
        if (!$column or $column === '*') {
            return $this -> addFunction('count_all', '*', $alias, false);
        }

        return $this -> addFunction('count', $column, $alias, $isDistinct);
    } // count()


    /**
     * @param $column
     * @param null $alias
     * @return $this
     */
    public function max($column, $alias = null)
    {
        return $this -> addFunction('max', $column, $alias);
    } // max()


    /**
     * @param $column
     * @param null $alias
     * @return $this
     */
    public function min($column, $alias = null)
    {
        return $this -> addFunction('min', $column, $alias);
    } // min()


    /**
     * @param $column
     * @param null $alias
     * @return $this
     */
    public function avg($column, $alias = null)
    {
        return $this -> addFunction('avg', $column, $alias);
    } // avg()


    /**
     * @param $column
     * @param null $alias
     * @return $this
     */
    public function sum($column, $alias = null)
    {
        return $this -> addFunction('sum', $column, $alias);
    } // sum()


    public function build()
    {
        if (1 === count($this -> functions)
            and 'count_all' === $this -> functions[0]['name'])
        {
            if ($this -> columns) {
                throw new Exception\InvalidClauseException('This type of query does not require the names of columns');
            }

            if ($this -> isDistinct) {
                throw new Exception\InvalidClauseException('This type of query does not require the DISTINCT keyword');
            }

            if (isset($this -> functions[0]['alias'])) {
                return sprintf('select count(*) as %s', $this -> functions[0]['alias']);
            }
            return 'select count(*)';
        }

        $columns = null;
        if (is_array($this -> columns)) {
            $temp = array();
            foreach($this -> columns as $alias => $column) {
                $temp[] = is_string($alias) ? sprintf('%s as %s', $column, $alias) : $column;
            }
            $columns = implode(', ', $temp);
        }
        else {
            $columns = $this -> columns;
        }

        $functions = null;
        if (is_array($this -> functions)) {
            $temp = array();
            foreach($this -> functions as $func) {

                if ('count_all' === $func['name']) {
                    throw new Exception\InvalidClauseException('Invalid call aggregate function COUNT');
                }

                $temp[] = sprintf('%s(%s%s)%s',
                    $func['name'],
                    $func['is_distinct'] ? 'distinct ' : '',
                    $func['column'],
                    $func['alias'] ? " as {$func['alias']}" : ''
                );
            }
            $functions = implode(', ', $temp);
        }

        if (!$columns and !$functions) {
            $columns = '*';
        }

        $result = sprintf('select%s %s%s',
            $this -> isDistinct ? ' distinct' : '',
            $columns ? "{$columns}, " : '',
            $functions ? $functions : ''
        );

        return rtrim($result, ', ');
    } // build()


    /**
     * @param $name
     * @param $column
     * @param null $alias
     * @param bool $isDistinct
     * @return $this
     */
    protected function addFunction($name, $column, $alias = null, $isDistinct = false)
    {
        $this -> functions[] = array(
            'name'        => $name,
            'column'      => $column,
            'alias'       => $alias,
            'is_distinct' => $isDistinct
        );
        return $this;
    } // addFunction()
} 