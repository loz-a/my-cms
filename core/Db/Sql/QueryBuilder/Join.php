<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;

class Join implements BuilderInterface
{
    use PredicatesTrait;

    const JOIN_LEFT  = 'left';
    const JOIN_RIGHT = 'right';
    const JOIN_FULL  = 'full';
    const JOIN_INNER = 'inner';

    protected $joinType = self::JOIN_INNER;

    protected $joinTable;

    protected $joinTableAlias;

    public function left($tableName = null, $alias = null)
    {
        $this -> joinType = self::JOIN_LEFT;

        if (null !== $tableName) {
            $this -> with($tableName, $alias);
        }

        return $this;
    } // left()


    public function right($tableName = null, $alias = null)
    {
        $this -> joinType = self::JOIN_RIGHT;

        if (null !== $tableName) {
            $this -> with($tableName, $alias);
        }

        return $this;
    } // right()


    public function full($tableName = null, $alias = null)
    {
        $this -> joinType = self::JOIN_FULL;

        if (null !== $tableName) {
            $this -> with($tableName, $alias);
        }

        return $this;
    } // full()


    public function with($tableName, $alias = null)
    {
        $this -> joinTable = $tableName;

        if (null !== $alias) {
            $this -> joinTableAlias = $alias;
        }
        return $this;
    } // with()


    public function on($predicate)
    {
        $this -> predicates[] = $predicate;
        return $this;
    } // on()


    public function build()
    {
        if (!$this -> joinTable) {
            throw new Exception\InvalidClauseException('Join table is undefined');
        }

        if (!$this -> predicates) {
            throw new Exception\InvalidClauseException('Join predicate is undefined');
        }

        $joinTable = $this -> joinTable;
        if (null !== $this -> joinTableAlias) {
            $joinTable = sprintf('%s as %s', $joinTable, $this -> joinTableAlias);
        }
        return sprintf('%s join %s on %s', $this -> joinType, $joinTable, $this -> join());
    }
}