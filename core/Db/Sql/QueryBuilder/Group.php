<?php
namespace Core\Db\Sql\QueryBuilder;


class Group implements BuilderInterface
{
    use PredicatesTrait;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @param $columns
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function by($columns)
    {
        if (is_array($columns)) {
            $this -> columns = $columns;
        }
        else if (is_object($columns)) {
            if (method_exists($columns, '__toString')) {
                $this -> columns = $columns -> __toString();
            }
            else if (method_exists($columns, 'toString')) {
                $this -> columns = $columns -> toString();
            }
        }
        else {
           $this -> columns = func_get_args();
        }
        return $this;
    } // by()


    /**
     * @param $column
     * @return $this
     */
    public function count($column)
    {
        $this -> predicates[] = sprintf('count(%s)', $column);
        return $this;
    } // count()


    /**
     * @param $column
     * @return $this
     */
    public function max($column)
    {
        $this -> predicates[] = sprintf('max(%s)', $column);
        return $this;
    } // max()


    /**
     * @param $column
     * @return $this
     */
    public function min($column)
    {
        $this -> predicates[] = sprintf('min(%s)', $column);
        return $this;
    } // min()


    /**
     * @param $column
     * @return $this
     */
    public function avg($column)
    {
        $this -> predicates[] = sprintf('avg(%s)', $column);
        return $this;
    } // avg()


    /**
     * @param $column
     * @return $this
     */
    public function sum($column)
    {
        $this -> predicates[] = sprintf('sum(%s)', $column);
        return $this;
    } // sum()


    public function build()
    {
        if (!$this -> columns) {
            throw new Exception\InvalidClauseException('Group by require the names of columns');
        }

        $result = sprintf('group by %s', implode(', ', $this -> columns));

        if ($this -> hasPredicates()) {
            $result = sprintf('%s having %s', $result, $this -> join());
        }
        return $result;
    } // build()

}