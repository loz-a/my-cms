<?php
namespace Core\Db\Sql\QueryBuilder\Where;

use Core\Db\Sql\QueryBuilder;

class SubqueryBuilder extends QueryBuilder
{
    const PREDICATE_EXISTS     = 'exists';
    const PREDICATE_NOT_EXISTS = 'not exists';
    const PREDICATE_SOME       = 'some';
    const PREDICATE_ANY        = 'any';
    const PREDICATE_ALL        = 'all';
    const PREDICATE_EMPTY      = '';

    /**
     * @var string
     */
    protected $subqueryPredicate = self::PREDICATE_EMPTY;

    /**
     * @return $this
     */
    public function exists()
    {
        $this -> subqueryPredicate = self::PREDICATE_EXISTS;
        return $this;
    } // exists()


    /**
     * @return $this
     */
    public function notExists()
    {
        $this -> subqueryPredicate = self::PREDICATE_NOT_EXISTS;
        return $this;
    } // notExists()


    /**
     * @return $this
     */
    public function some()
    {
        $this -> subqueryPredicate = self::PREDICATE_SOME;
        return $this;
    } // some()


    /**
     * @return $this
     */
    public function any()
    {
        $this -> subqueryPredicate = self::PREDICATE_ANY;
        return $this;
    } // any()


    /**
     * @return $this
     */
    public function all()
    {
        $this -> subqueryPredicate = self::PREDICATE_ALL;
        return $this;
    } // all()


    public function build()
    {
        list($sql, $boundParams) = parent::build();

        $resultParams  = array();
        foreach (array('group', 'where', 'join') as $operator) {
            $argName = sprintf(":{$operator}_%s_", strtolower(str_replace(' ', '', $this -> subqueryPredicate)));

            foreach ($boundParams as $k => $v) {
                if (1 === strpos($k, $operator, 1)) {
                    $resultParams[str_replace(":{$operator}_", $argName, $k)] = $v;
                }
            }

            $sql = str_replace(":{$operator}_", $argName, $sql);
        }
        $this -> params() -> reset() -> set($resultParams);

        $sql = trim(sprintf('%s (%s)', $this -> subqueryPredicate, $sql));

        return array($sql, $this -> params() -> toArray());
    } // build()

} 