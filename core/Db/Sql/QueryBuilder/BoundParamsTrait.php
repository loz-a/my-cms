<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\Params\Params;

trait BoundParamsTrait
{

    /**
     * @var Params
     */
    protected $boundParams;

    /**
     * @return Params
     */
    public function params()
    {
        if (null === $this -> boundParams) {
            $this -> boundParams = new Params();
            $this -> boundParams -> setKeyPrefix(basename(__CLASS__));
        }
        return $this -> boundParams;
    } // params()


}