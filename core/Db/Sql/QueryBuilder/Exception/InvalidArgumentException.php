<?php
namespace Core\Db\Sql\QueryBuilder\Exception;

class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

} 