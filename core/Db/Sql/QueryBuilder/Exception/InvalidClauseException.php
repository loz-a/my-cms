<?php
namespace Core\Db\Sql\QueryBuilder\Exception;

class InvalidClauseException extends \DomainException
    implements ExceptionInterface
{

} 