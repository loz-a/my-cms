<?php
namespace Core\Db\Sql\QueryBuilder;


interface BuilderInterface
{
    public function build();
} 