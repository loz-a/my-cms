<?php
namespace Core\Db\Sql\QueryBuilder\From;


trait CheckTableNameTrait
{
    public function hasTableName()
    {
        return !is_null($this -> table);
    } // hasTableName()
} 