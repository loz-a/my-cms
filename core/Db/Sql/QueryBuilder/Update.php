<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder\From\CheckTableNameInterface;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameTrait;

class Update implements BuilderInterface, CheckTableNameInterface
{
    use BoundParamsTrait, CheckTableNameTrait;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $set;

    /**
     * @var Where
     */
    protected $where;

    /**
     * @param null $table
     */
    public function __construct($table = null)
    {
        if ($table) {
            $this -> table($table);
        }
    } // __construct()


    /**
     * @param $table
     * @return $this
     */
    public function table($table)
    {
        $this -> table = $table;
        return $this;
    } // table()


    /**
     * @param array $values
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function set(array $values)
    {
        $this -> set = $values;
        return $this;
    } // set()


    /**
     * @return Where
     */
    public function where()
    {
        if (null === $this -> where) {
            $this -> where = new Where($this);
        }
        return $this -> where;
    } // where()


    public function build()
    {
        if (null === $this -> table) {
            throw new Exception\InvalidClauseException('Table name is undefined');
        }

        if (null === $this -> set) {
            throw new Exception\InvalidClauseException('Values is undefined');
        }

        $set          = array();
        $placeholders = array();

        foreach ($this -> set as $k => $v) {
            if (!is_string($k)) {
                throw new Exception\InvalidArgumentException('set() expects a string for the value key');
            }
            $placeholder = $this -> params() -> key();

            $set[] = sprintf('%s = %s', $k, $placeholder);
            $this -> params() -> set($placeholder, $v);
        }

        $result = sprintf('update %s set %s', $this -> table, implode(', ', $set));

        if (null !== $this -> where) {
            $result = sprintf('%s %s', $result, $this -> where -> build());
            $this -> params() -> set($this -> where -> params() -> toArray());
        }

        return array($result, $this -> params() -> toArray());
    } // build()
} 