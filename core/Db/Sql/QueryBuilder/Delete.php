<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder\From\CheckTableNameInterface;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameTrait;

class Delete implements BuilderInterface, CheckTableNameInterface
{
    use BoundParamsTrait, CheckTableNameTrait;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var Where
     */
    protected $where;

    /**
     * @param null $table
     */
    public function __construct($table = null)
    {
        if ($table) {
            $this -> from($table);
        }
    } // __construct()


    /**
     * @param $table
     * @return $this
     */
    public function from($table)
    {
        $this -> table = $table;
        return $this;
    } // from()


    /**
     * @return Where
     */
    public function where()
    {
        if (null === $this -> where) {
            $this -> where = new Where($this);
        }
        return $this -> where;
    } // where()


    public function build()
    {
        if (null === $this -> table) {
            throw new Exception\InvalidClauseException('Table name is undefined');
        }

        $result = sprintf('delete from %s', $this -> table);

        if (null !== $this -> where) {
            $result = sprintf('%s %s', $result, $this -> where -> build());
            $this -> params() -> set($this -> where -> params() -> toArray());
        }

        return array($result, $this -> params() -> toArray());
    } // build()

} 