<?php
namespace Core\Db\Sql\QueryBuilder;


class Order implements BuilderInterface
{
    use QueryBuilderAwareTrait;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @param $column
     * @return $this
     */
    public function asc($column)
    {
        $this -> columns[] = sprintf('%s asc', $column);
        return $this;
    } // asc()


    /**
     * @param $column
     * @return $this
     */
    public function desc($column)
    {
        $this -> columns[] = sprintf('%s desc', $column);
        return $this;
    } // desc()


    /**
     * @return string
     * @throws Exception\InvalidClauseException
     */
    public function build()
    {
        if (!$this -> columns) {
            throw new Exception\InvalidClauseException('Order by require the names of columns');
        }
        return sprintf('order by %s', implode(', ', $this -> columns));
    } // build()

} 