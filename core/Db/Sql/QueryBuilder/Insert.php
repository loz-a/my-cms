<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameInterface;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameTrait;

class Insert implements BuilderInterface, CheckTableNameInterface
{
    use CheckTableNameTrait;
    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @var array|QueryBuilder
     */
    protected $values;


    public function __construct($table = null)
    {
        if ($table) {
            $this -> into($table);
        }
    } // __construct()

    /**
     * @param $table
     * @return $this
     */
    public function into($table)
    {
        $this -> table = $table;
        return $this;
    } // into()


    /**
     * @param array $columns
     * @return $this
     */
    public function columns($columns)
    {
        if (!is_array($columns)) {
            $columns = func_get_args();
        }

        $this -> columns = $columns;
        return $this;
    } // columns()


    /**
     * @param array|QueryBuilder $values
     * @return $this
     */
    public function values($values)
    {
        if (is_array($values) or $values instanceof QueryBuilder) {
            $this -> values = $values;
        }
        else if (is_object($values) and method_exists($values, 'qb')) {
            $this -> values = $values -> qb();
        }
        else {
            $this -> values = func_get_args();
        }
        return $this;
    } // values()


    public function build()
    {
        if (null === $this -> table) {
            throw new Exception\InvalidClauseException('Table name is undefined');
        }
        $result = sprintf('insert into %s', $this -> table);

        if (null === $this -> values) {
            $result = sprintf('%s default values', $result);
            return array($result, array());
        }

        if ($this -> values instanceof QueryBuilder) {
            $subquery = $this -> values -> build();

            $result = (null === $this -> columns)
                ? sprintf('%s %s', $result, $subquery[0])
                : sprintf('%s (%s) %s', $result, implode(', ', $this -> columns), $subquery[0]);

            return array($result, $subquery[1]);
        }

        $columnsCount = count($this -> columns);
        $valuesCount  = count($this -> values);

        if ($columnsCount and $valuesCount and $columnsCount !== $valuesCount) {
            throw new Exception\InvalidClauseException('Columns number should be equal the number of values');
        }

        $placeholder = substr(str_repeat('?,', $valuesCount), 0, -1);

        $result = (null === $this -> columns)
            ? sprintf('%s values (%s)', $result, $placeholder)
            : sprintf('%s (%s) values (%s)', $result, implode(', ', $this -> columns), $placeholder);

        return array($result, $this -> values);
    } // build()
} 