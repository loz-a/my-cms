<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;

trait PredicatesTrait
{
    use BoundParamsTrait, QueryBuilderAwareTrait {
        QueryBuilderAwareTrait::__call as qbCall;
    }

    /**
     * @var array
     */
    protected $predicates;

    /**
     * @var array
     */
    protected $predicatesSpecifications = array(
        'equal'      => '%s = %s',
        'notEqual'   => '%s != %s',
        'like'       => '%s like %s',
        'notLike'    => '%s not like %s',
        'gt'         => '%s > %s',
        'gte'        => '%s >= %s',
        'lt'         => '%s < %s',
        'lte'        => '%s <= %s',
        'in'         => '%s in (%s)',
        'notIn'      => '%s not in (%s)',
        'between'    => '%s between %s and %s',
        'notBetween' => '%s not between %s and %s',
        'isNull'     => '%s is null',
        'isNotNull'  => '%s is not null',
        'predicate'  => ''
    );


    protected $predicatesShortSpecifications = array(
        'equal'      => '= %s',
        'notEqual'   => '!= %s',
        'gt'         => '> %s',
        'gte'        => '>= %s',
        'lt'         => '< %s',
        'lte'        => '<= %s',
        'in'         => 'in (%s)',
        'notIn'      => 'not in (%s)',
        'between'    => 'between %s and %s',
        'notBetween' => 'not between %s and %s'
    );


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function equal($identifier, $value = null)
    {
        return $this -> addPredicate('equal', $identifier, $value);
    } // equal()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function notEqual($identifier, $value = null)
    {
        return $this -> addPredicate('notEqual', $identifier, $value);
    } // notEqual()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function like($identifier, $value = null)
    {
        return $this -> addPredicate('like', $identifier, $value);
    } // like()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function notLike($identifier, $value = null)
    {
        return $this -> addPredicate('notLike', $identifier, $value);
    } // notLike()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function gt($identifier, $value = null)
    {
        return $this -> addPredicate('gt', $identifier, $value);
    } // gt()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function gte($identifier, $value = null)
    {
        return $this -> addPredicate('gte', $identifier, $value);
    } // gte()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function lt($identifier, $value = null)
    {
        return $this -> addPredicate('lt', $identifier, $value);
    } // lt()


    /**
     * @param $identifier
     * @param $value
     * @return $this
     */
    public function lte($identifier, $value = null)
    {
        return $this -> addPredicate('lte', $identifier, $value);
    } // lte()


    /**
     * @param $identifier
     * @return $this
     */
    public function isNull($identifier)
    {
        return $this -> addPredicate('isNull', $identifier);
    } // isNull()


    /**
     * @param $identifier
     * @return $this
     */
    public function isNotNull($identifier)
    {
        return $this -> addPredicate('isNotNull', $identifier);
    } // isNotNull()


    /**
     * @param $identifier
     * @param array $values
     * @return $this
     */
    public function in($identifier, array $values)
    {
        return $this -> addPredicate('in', $identifier, $values);
    } // in()


    /**
     * @param $identifier
     * @param array $values
     * @return $this
     */
    public function notIn($identifier, array $values)
    {
        return $this -> addPredicate('notIn', $identifier, $values);
    } // notIn()


    /**
     * @param $identifier
     * @param $min
     * @param $max
     * @return $this
     */
    public function between($identifier, $min, $max)
    {
        return $this -> addPredicate('between', $identifier, compact('min', 'max'));
    } // between()


    /**
     * @param $identifier
     * @param $min
     * @param $max
     * @return $this
     */
    public function notBetween($identifier, $min, $max)
    {
        return $this -> addPredicate('notBetween', $identifier, compact('min', 'max'));
    } // notBetween()


    /**
     * @param $clause
     * @param array $boundParams
     * @return $this
     */
    public function predicate($clause, array $boundParams = null)
    {
        if (null !== $boundParams) {
           $this -> params() -> set($boundParams);
        }

        $clause = trim($clause);

        if (strlen($clause) > 0) {
            $this -> predicates[] = $clause;
        }
        return $this;
    } // predicate()


    /**
     * @param BuilderInterface $subqueryBuilder
     * @return $this
     */
    public function subquery(BuilderInterface $subqueryBuilder)
    {
        if (method_exists($subqueryBuilder, 'qb')) {
            $subqueryBuilder = $subqueryBuilder -> qb();
        }

        $result = $subqueryBuilder -> build();
        $this -> predicates[] = $result[0];
        $this -> params() -> set($result[1]);

        return $this;
    } // subquery()


    /**
     * @return $this
     */
    public function resetPredicates()
    {
        $this -> predicates = array();
        $this -> params() -> reset();
        return $this;
    } // resetPredicate()


    /**
     * @return bool
     */
    public function hasPredicates()
    {
        return is_array($this -> predicates) and count($this -> predicates);
    } // hasPredicates()


    /**
     * @return string
     */
    protected function join()
    {
        if ('and' === $this -> predicates[0] or 'or' === $this -> predicates[0]) {
            array_shift($this -> predicates);
        }

        $lastIdx = count($this -> predicates) - 1;
        if ('and' === $this -> predicates[$lastIdx] or 'or' === $this -> predicates[$lastIdx]) {
            array_pop($this -> predicates);
        }

        return implode(' ', $this -> predicates);
    } // join()


    /**
     * @param $name
     * @param $args
     * @return mixed
     * @throws Exception\BadMethodCallException
     */
    public function __call($name, $args)
    {
        if ('order' === $name
            and method_exists($this -> qb(), 'order')
        ) {
            return $this -> qb() -> order();
        }

        $operator = (strpos($name, 'and') === 0)
            ? 'and'
            : (strpos($name, 'or') === 0 ? 'or' : null);

        if (null !== $operator) {
            $method = lcfirst(ltrim($name, $operator));

            $this -> predicates[] = $operator;
            return call_user_func_array(array($this, $method), $args);
        }

        return $this -> qbCall($name, $args);
    } // __call()


    /**
     * @param string $method
     * @param string $identifier
     * @param null $value
     * @return $this
     */
    protected function addPredicate($method, $identifier, $value = null)
    {
        return (null === $value and array_key_exists($method, $this -> predicatesShortSpecifications))
            ? $this -> addShortPredicate($method, $identifier)
            : $this -> addLongPredicate($method, $identifier, $value);
    } // addPredicate()


    /**
     * @param string $method
     * @param string $identifier
     * @param null|string|array $value
     * @return $this
     * @throws Exception\InvalidClauseException
     */
    protected function addLongPredicate($method, $identifier, $value = null)
    {
        if (!array_key_exists($method, $this -> predicatesSpecifications)) {
            throw new Exception\InvalidClauseException(sprintf('Invalid predicate %s', $method));
        }

        $clause = null;
        if (is_array($value)) {

            if (isset($value['max']) and isset($value['min'])) {
                $boundMinKey = $this -> params() -> key('min');
                $boundMaxKey = $this -> params() -> key('max');

                $clause = sprintf($this -> predicatesSpecifications[$method], $identifier, $boundMinKey, $boundMaxKey);

                $this -> params()
                    -> set($boundMinKey, $value['min'])
                    -> set($boundMaxKey, $value['max']);
            }
            else {
                $boundKeys = array();
                foreach ($value as $val) {
                    $boundKey    = $this -> params() -> key('in');
                    $boundKeys[] = $boundKey;
                    $this -> params() -> set($boundKey, $val);
                }
                $clause = sprintf($this -> predicatesSpecifications[$method], $identifier, implode(', ', $boundKeys));
            }
        }
        else if (is_null($value)) {
            $clause = sprintf($this -> predicatesSpecifications[$method], $identifier);
        }
        else if ($value instanceof BuilderInterface) {
            if (method_exists($value, 'qb')) {
                $value = $value -> qb();
            }

            list($sql, $boundParams) = $value -> build();
            $clause = sprintf($this -> predicatesSpecifications[$method], $identifier, $sql);
            $this -> params() -> set($boundParams);
        }
        else {
            $boundKey = $this -> params() -> key();
            $clause   = sprintf($this -> predicatesSpecifications[$method], $identifier, $boundKey);
            $this -> params() -> set($boundKey, $value);
        }
        $this -> predicates[] = $clause;

        return $this;
    } // addLongPredicate()


    /**
     * @param $method
     * @param $value
     * @return $this
     */
    protected function addShortPredicate($method, $value)
    {
        if (is_array($value)) {

            if (isset($value['max']) and isset($value['min'])) {
                $boundMinKey = $this -> params() -> key('min');
                $boundMaxKey = $this -> params() -> key('max');

                $clause = sprintf($this -> predicatesSpecifications[$method], $boundMinKey, $boundMaxKey);

                $this -> params()
                    -> set($boundMinKey, $value['min'])
                    -> set($boundMaxKey, $value['max']);
            }
            else {
                $boundKeys = array();
                foreach ($value as $val) {
                    $boundKey    = $this -> params() -> key('in');
                    $boundKeys[] = $boundKey;
                    $this -> params() -> set($boundKey, $val);
                }
                $clause = sprintf($this -> predicatesSpecifications[$method], implode(', ', $boundKeys));
            }
        }
        else {
            $boundKey = $this -> params() -> key();
            $clause   = sprintf($this -> predicatesShortSpecifications[$method], $boundKey);
            $this -> params() -> set($boundKey, $value);
        }
        $this -> predicates[] = $clause;

        return $this;
    } // addShortPredicate()


    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this -> build();
    } // __toString()


    abstract public function build();
} 