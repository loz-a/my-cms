<?php
namespace Core\Stdlib;


class Params
{
    /**
     * @var null|array
     */
    protected $data;

    /**
     * @param string|array $key
     * @param null $value
     * @return $this
     */
    public function set($key, $value = null)
    {
        if (null === $this -> data) {
            $this -> data = array();
        }

        if (is_array($key)) {
            $this -> data = array_merge($this -> data, $key);
            return $this;
        }

        $key = (string) $key;
        $this -> data[$key] = (string) $value;
        return $this;
    } // set()


    /**
     * @param null string|array $key
     * @param null $default
     * @return array|null
     */
    public function get($key = null, $default = null)
    {
        if (null === $this -> data) {
            $this -> data = array();
        }

        if (null === $key) {
            return $this -> data;
        }
        $key = (string) $key;
        return array_key_exists($key, $this -> data) ? $this -> data[$key] : $default;
    } // get()


    /**
     * @param callable $callback
     * @return array
     */
    public function filter(\Closure $callback)
    {
        $result = array();
        foreach ($this -> data as $key => $value) {
            if ($callback($value, $key)) {
                $result[$key] = $value;
            }
        }
        return $result;
    } // filter()
    

    /**
     * @return $this
     */
    public function reset()
    {
        $this -> data = array();
        return $this;
    } // reset()


    /**
     * @return array
     */
    public function toArray()
    {
        return (null === $this -> data) ? array() : $this -> data;
    } // toArray()


    public function count()
    {
        return is_array($this -> data) ? count($this -> data) : 0;
    } // count()

} 