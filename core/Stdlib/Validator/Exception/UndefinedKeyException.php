<?php
namespace Core\Stdlib\Validator\Exception;

class UndefinedKeyException extends \Exception
    implements ExceptionInterface
{

} 