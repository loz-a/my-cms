<?php
namespace Core\Stdlib\Validator;

use Zend\Validator\AbstractValidator;

abstract class AbstractRecord extends AbstractValidator
{
    /**
     * Error constants
     */
    const ERROR_NO_RECORD_FOUND = 'noRecordFound';
    const ERROR_RECORD_FOUND    = 'recordFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_NO_RECORD_FOUND => "No record matching '%value%' was found",
        self::ERROR_RECORD_FOUND    => "A record matching '%value%' was found",
    );

    /**
     * @var mixed
     */
    protected $mapper;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $exclude;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        if ( array_key_exists('key', $options) ) {
            $this -> setKey($options['key']);
        }

        if ( array_key_exists('mapper', $options) ) {
            $this -> setMapper($options['mapper']);
        }

        if ( array_key_exists('exclude', $options) ) {
            $this -> setExclude($options['exclude']);
        }

        parent::__construct($options);
    } // __construct()


    /**
     * @param $mapper
     * @return mixed
     */
    abstract public function setMapper($mapper);

    /**
     * @return mixed
     * @throws Exception\UndefinedMapperException
     */
    public function getMapper()
    {
        if ( null === $this -> mapper ) {
            throw new Exception\UndefinedMapperException('Mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    /**
     * @return string
     * @throws Exception\UndefinedKeyException
     */
    public function getKey()
    {
        if ( null === $this -> key ) {
            throw new Exception\UndefinedKeyException('No key provided');
        }
        return $this -> key;
    } // getKey()


    /**
     * @param string $key
     * @return AbstractRecord
     */
    public function setKey($key)
    {
        $this -> key = (string) $key;
        return $this;
    } // setKey()


    /**
     * @return string
     */
    public function getExclude()
    {
        return $this -> exclude;
    } // getExclude()


    /**
     * @param string $exclude
     * @return $this
     */
    public function setExclude($exclude)
    {
        $this -> exclude = (string) $exclude;
        return $this;
    } // setExclude()

} // AbstractRecord
