<?php
namespace Core\Stdlib\Hydrator;

use Zend\Stdlib\Hydrator\HydratorInterface;

trait HydratorAwareTrait
{
    /**
     * Hydrator instance
     *
     * @var HydratorInterface
     * @access protected
     */
    protected $hydrator = null;

    /**
     * @param HydratorInterface $hydrator
     * @return $this
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;

        return $this;
    }

    /**
     * @return HydratorInterface
     * @throws Exception\UndefinedHydratorException
     */
    public function getHydrator()
    {
        if (null === $this -> hydrator) {
            throw new Exception\UndefinedHydratorException('Hydrator is undefined');
        }

        return $this->hydrator;
    }
}
