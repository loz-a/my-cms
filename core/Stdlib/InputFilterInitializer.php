<?php
namespace Core\Stdlib;

use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\InitializableInterface;

class InputFilterInitializer implements InitializerInterface
{
    /**
     * Initialize
     *
     * @param $instance
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        if ($instance instanceof InputFilterInterface) {

            if ($instance instanceof ServiceLocatorAwareInterface) {
                $instance -> setServiceLocator($serviceLocator);
            }

            if ($instance instanceof InitializableInterface) {
                $instance -> init();
            }
        }

    }

} 