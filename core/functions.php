<?php
use Core\App;
use Core\Db\Sql;

/**
 *  translate
 */
if (!function_exists('t')) {
    function t($message) {
        return $message;
    } // t()
}


if (!function_exists('esc')) {
    /**
     * Escape variable for use in View
     *
     * Example of usage
     *     esc($_GET['name']);
     *     esc($_GET['name'], ENT_QUOTES);
     *
     * @param string $variable
     * @param int $flags
     * @return string
     */
    function esc($variable, $flags = ENT_HTML5) {
        return htmlspecialchars($variable, $flags, "UTF-8");
    }
}


if (!function_exists('cast_to_type')) {

    function cast_to_type($value, $type) {
        switch(strtolower($type)) {
            case 'bool':
            case 'boolean':
                $filtered = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                $value = is_null($filtered) ? (bool) $value : $filtered;
                break;
            case 'int':
            case 'integer':
                $value = (int) $value;
                break;
            case 'float':
                $value = (float) $value;
                break;
            case 'str':
            case 'string':
                $value = (string) $value;
                break;
            case 'sanitize_string':
            case 'sanitize_str':
                $value = filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
                break;
            case 'escape_string':
            case 'escape_str':
                $value = filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
                break;
            default:
                throw new \Core\Exception\InvalidTypeException(sprintf('Invalid type "%s" in for value "%s"', $type, $value));
        }
        return $value;
    } // cast_to_type()
}


if (!function_exists('flash_messenger')) {
    /**
     * @return Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    function flash_messenger() {
        return App::getInstance() -> getServiceManager() -> get('flashMessenger');
    }
}


if (!function_exists('url_for')) {
    /**
     * @param null $route
     * @param array $params
     * @param array $options
     * @return string
     */
    function url_for($route = null, array $params = array(), array $options = array()) {
        return App::getInstance() -> getRoutesManager() -> urlFor($route, $params, $options);
    }
}


if (!function_exists('qb')) {
    /**
     * @return Sql\QueryBuilder
     */
    function qb() {
        return new Sql\QueryBuilder();
    }
}

if (!function_exists('fsb')) {
    /**
     * @return Sql\QueryBuilder\From\SubqueryBuilder
     */
    function fsb() {
        return new Sql\QueryBuilder\From\SubqueryBuilder();
    }
}

if (!function_exists('wsb')) {
    /**
     * @return Sql\QueryBuilder\Where\SubqueryBuilder
     */
    function wsb() {
        return new Sql\QueryBuilder\Where\SubqueryBuilder();
    }
}


if (!function_exists('render')) {

    /**
     * @param $nameOrModel
     * @param null $model
     * @return string
     */
    function render($nameOrModel, $model = array()) {
        return App::getInstance() -> getServiceManager() -> get('view') -> getRenderer() -> render($nameOrModel, $model);
    }
}


//
//
//if (!function_exists('err_handler')) {
//    function err_handler(App $app, $templateIndex, $template)
//    {
//        $container = $app -> getContainer();
//        $container -> get('view.map_resolver')[$templateIndex] = $template;
//        $content = $container -> get('view.php_renderer') -> render($templateIndex);
//        $container -> get('response') -> setContent($content);
//    }
//}