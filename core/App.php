<?php
namespace Core;

use Core\App\AppAwareInterface;
use Core\Annotations;
use Core\App\Controller\ControllerInterface;
use Core\App\Exception\InvalidControllerException;
use Core\App\Module;
use Core\App\EventManagerAwareTrait;
use Core\App\RequestParams;
use Core\App\Route;
use Zend\Config\Config;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;
use Zend\ServiceManager;
use Core\Stdlib\Params;
use Core\View\Model\ModelTypeInterface;

class App implements
    ServiceManager\ServiceManagerAwareInterface,
    EventManagerAwareInterface,
    Module\ModuleManagerAwareInterface,
    Route\RoutesManagerAwareInterface
{
    use SingletonTrait,
        EventManagerAwareTrait,
        Module\ModuleManagerAwareTrait,
        Route\RoutesManagerAwareTrait;

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * @param ServiceManager $serviceManager
     * @return $this
     */
    public function setServiceManager(ServiceManager\ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
        return $this;
    } // setServiceManager()


    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this -> serviceManager;
    } // services()


    /**
     * @var Params
     */
    protected $params;

    /**
     * @return $this
     */
    protected function loadModules()
    {
        $modulesConfig = $this -> getServiceManager() -> get('config') -> modules;
        $moduleManager = new Module\Manager($modulesConfig, $this);
        $this -> setModuleManager($moduleManager);
        $this -> getModuleManager() -> loadModules();

        return $this;
    } // loadModules()


    /**
     * @return $this
     */
    protected function initGlobal()
    {
        if (class_exists('\App\Init')) {
            (new \App\Init()) -> __invoke($this);
        }
        return $this;
    } // initGlobal()


    /**
     * @param array $config
     * @return $this
     */
    public function init(array $config = array())
    {
        $smConfig = isset($config['service_manager']) ? $config['service_manager'] : array();
        $serviceManager = new ServiceManager\ServiceManager(new ServiceManager\Config($smConfig));

        $serviceManager -> setService('config', new Config($config));
        $serviceManager -> setService('app', $this);
        $serviceManager -> setService('request', new Request());
        $serviceManager -> setService('response', new Response());


        $this
            -> setServiceManager($serviceManager)
            -> setRoutesManager(new Route\Manager($this))
            -> initGlobal()
            -> loadModules();

        return $this;
    } // init()


    /**
     * @param string| $exclude
     * @return Params
     */
    public function getParams($exclude = null)
    {
        if (null === $this -> params) {
            $this -> params = new RequestParams($this -> getServiceManager() -> get('request'));
        }

        if ($exclude) {

            if (is_string($exclude)) {
                $exclude = array($exclude);
            }
            $exclude = array_flip($exclude);
            $params = $this -> params -> toArray();
            $this -> params -> reset() -> set(array_diff_key($params, $exclude));
        }
        return $this -> params;
    } // getParams()


    public function getViewModel($values)
    {
        if (is_scalar($values)) {
            $values = compact('values');
        }

        $viewModel = null;
        if (is_array($values)) {
            $viewModel = $this -> getServiceManager() -> get('view_model.view');
            $viewModel -> exchangeArray($values);
            return $viewModel;
        }
        elseif (is_object($values)) {
            $viewModel = $this -> getServiceManager() -> get('view_model.view');
             (method_exists($values, 'toArray'))
                ? $viewModel -> exchangeArray($values -> toArray())
                : $viewModel -> exchangeArray(get_object_vars($values));
            return $viewModel;
        }
        return $values;
    } // getViewModel()


    public function run()
    {
        $sm       = $this -> getServiceManager();
        $response = $sm -> get('response');

        try {

            $this -> trigger('begin');

            route:
                $controllerName = $this
                    -> getRoutesManager()
                    -> route($sm -> get('request'))
                    -> getParam('controller', 'index.controller.index');

                $controller = $sm -> get($controllerName);

                if ($controller instanceof AppAwareInterface or method_exists($controller, 'setApplication')) {
                    $controller -> setApplication($this);
                }

                if ($controller instanceof Annotations\AnnotationsManagerAwareInterface or method_exists($controller, 'setAnnotationsManager')) {
                    $controller -> setAnnotationsManager(new Annotations\Manager());
                    $this -> getEventManager() -> attachAggregate(new Annotations\Events());
                }

                if (!$controller instanceof ControllerInterface) {
                    throw new InvalidControllerException('Controller must implement Core\App\Controller\ControllerInterface');
                }
                $result = $controller -> callAction($this);

                if (!$result instanceof Response) {

                    $viewModel = ($result instanceof ModelTypeInterface)
                                    ? $result : $this -> getViewModel($result);

                    $content   = $sm -> get('view') -> render($viewModel);
                    $response -> setContent($content);
                }

        } catch (App\Exception\HaltException $e) {
            // Handle a halt condition
            $this -> trigger('halt');
        } catch (App\Exception\PassException $e) {
            // Pass handling on to next route that matches
            goto route;
        } catch (App\Exception\PageNotFoundException $e) {
            // Handle a 404 condition
            $this -> trigger('404');
        } catch (App\Exception\InvalidControllerException $e) {
            // Handle situation where controller is invalid
            $this -> trigger('500');
        } catch (\Exception $e) {
            // Handle all other exceptions
//            var_dump(__FILE__ . '. Line: ' . __LINE__);var_dump($e);die;
            $this -> trigger('500', ['exception' => $e]);
        }

        $this -> trigger('finish');
        $response -> send();
    } // run()

} 