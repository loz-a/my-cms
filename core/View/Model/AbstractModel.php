<?php
namespace Core\View\Model;


class AbstractModel implements \ArrayAccess, \Countable, \IteratorAggregate, \Serializable
{
    /**
     * @var array
     */
    protected $data = array();

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this -> data[$offset]);
    } // offsetExists()


    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return array_key_exists($offset, $this -> data) ? $this -> data[$offset]: null;
    } // offsetGet()


    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        $this -> data[$offset] = $value;
    } // offsetSet()


    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        if (isset($this -> data[$offset])) {
            unset($this -> data[$offset]);
        }
    } // offsetUnset()


    /**
     * @return int
     */
    public function count()
    {
        return count($this -> data);
    } // count()


    /**
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this -> data);
    } // getIterator()


    /**
     * @return string
     */
    public function serialize()
    {
        return serialize($this -> data);
    } // serialize()


    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $this -> data = unserialize($serialized);
    } // unserialize()


    /**
     * @param $input
     * @return array
     */
    public function exchangeArray($input)
    {
        $result = $this -> data;

        if (is_array($input)) {
            $this -> data = $input;
        }

        if (is_object($input) and method_exists($input, '__toArray')) {
            $this -> data = $input -> __toArray();
        }

        if (is_object($input) and method_exists($input, 'toArray')) {
            $this -> data = $input -> toArray();
        }

        $this -> data = (array) $input;
        return $result;
    } // exchangeArray()


    /**
     * @param array $input
     * @return $this
     */
    public function fromArray(array $input)
    {
        $this -> data = array_merge($this -> data, $input);
        return $this;
    } // fromArray()


    /**
     * alias for toArray method
     * @return array
     */
    public function getArrayCopy()
    {
        return $this -> toArray();
    } // getArrayCopy()


    /**
     * @return array
     */
    public function toArray()
    {
        return $this -> data;
    } // toArray()
} 