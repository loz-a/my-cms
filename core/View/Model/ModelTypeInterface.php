<?php
namespace Core\View\Model;

interface ModelTypeInterface
{
    /**
     * @return string
     */
    public function modelType();
} 