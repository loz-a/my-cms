<?php
namespace Core\View\Model;


class JsonErrorModel extends JsonModel
{
    protected $messages = array();

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this -> messages;
    } // getMessages()


    /**
     * @return bool
     */
    public function hasMessages()
    {
        return (bool) count($this -> messages);
    } // hasMessages()


    /**
     * @param $key
     * @param $msg
     * @return $this
     */
    public function addMessage($key, $msg)
    {
        $this -> messages[$key] = $msg;
        return $this;
    } // addMessage()


    /**
     * @param array $messages
     * @return $this
     */
    public function setMessages(array $messages)
    {
        $this -> messages = $messages;
        return $this;
    } // setMessages()


    /**
     * @return $this
     */
    public function clearMessages()
    {
        $this -> messages = array();
        return $this;
    } // clearMessages()


    public function toArray()
    {
        $result = array();

        foreach ($this -> data as $key => $value) {
            $result[$key]['value'] = $value;

            if (isset($this -> messages[$key])) {
                $result[$key]['messages'] = array_values($this -> messages[$key]);
            }
        }

        return $result;
    } // toArray()
} 