<?php
namespace Core\View\Model;

class ViewModel extends AbstractModel implements ModelTypeInterface
{
    const MODEL_TYPE = 'php';

    /**
     * @var string
     */
    protected $templateName;

    /**
     * @param string $template
     * @return $this
     */
    public function setTemplateName($template)
    {
        $this -> templateName = (string) $template;
        return $this;
    } // setTemplate()


    /**
     * @return string
     */
    public function getTemplateName()
    {
        return $this -> templateName;
    } // getTemplate()


    /**
     * @return string
     */
    public function modelType()
    {
        return self::MODEL_TYPE;
    } // modelType()

} // ViewModel
