<?php
namespace Core\View\Exception;

/**
 * Exception raised when an invalid method call is made
 *
 * @subpackage Exception
 */
class BadMethodCallException extends \BadMethodCallException implements Exception
{
}
