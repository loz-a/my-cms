<?php

namespace Core\View\Exception;

class RuntimeException extends \Exception implements Exception
{
}
