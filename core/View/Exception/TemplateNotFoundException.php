<?php

namespace Core\View\Exception;

/**
 * Exception raised when a matching template file may not be found
 *
 * @subpackage Exception
 */
class TemplateNotFoundException extends \Exception implements Exception
{
}
