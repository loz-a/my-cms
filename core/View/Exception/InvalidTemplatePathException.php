<?php
namespace Core\View\Exception;

/**
 * Exception raised when an invalid template path is provided
 *
 * @subpackage Exception
 */
class InvalidTemplatePathException extends \Exception implements Exception
{
}
