<?php
namespace Core\View\Widget;

use Core\App as Application;
use Core\View\Exception\InvalidViewException;
use Core\View\ViewInterface;

abstract class AbstractWidget
{
    /**
     * @var ViewInterface
     */
    protected $view;

    abstract public function __invoke(Application $app);


    /**
     * @param ViewInterface $view
     * @return $this
     */
    public function setView(ViewInterface $view)
    {
        $this -> view = $view;
        return $this;
    } // setView()


    public function getView()
    {
        if (null === $this -> view) {
            throw new InvalidViewException('View is undefined');
        }
        return $this -> view;
    } // getView()
} 