<?php
use Core\App;
use Core\View\Helper\CaptureContent;

if (!function_exists('capture_content')) {
    /**
     * @return \Core\View\Helper\CaptureContent
     */
    function capture_content() {
        return CaptureContent::getInstance();
    }
}


if (!function_exists('url_for')) {
    /**
     * @param null $route
     * @param array $params
     * @param array $options
     * @return string
     */
    function url_for($route = null, array $params = [], array $options = []) {
        return App::getInstance() -> getRoutesManager() -> urlFor($route, $params, $options);
    }
}


if (!function_exists('base_url')) {
    /**
     * @return string
     */
    function base_url() {
        static $baseUrl;

        if (null === $baseUrl) {
            $baseUrl = App::getInstance() -> getRoutesManager() -> getRouteStack() -> getBaseUrl();
        }
        return $baseUrl;
    }
}


if (!function_exists('widget')) {
    /**
     * @param $name
     * @return \Core\View\Widget\AbstractWidget
     */
    function widget($name) {
        $app = App::getInstance();
        return $app -> getServiceManager() -> get('view.widgets') -> get($name) -> __invoke($app);
    }
}
