<?php
namespace Core\View\Annotations;

use Core\App;
use Core\View\Model\ViewModel;
use Core\View\ViewEvent as Event;

/**
 *  Annotation example:
 *  @Core.View.Annotations.Layout index/layout/layout
 */
class Layout
{
    public function after($tagContent)
    {
        $em = App::getInstance() -> getServiceManager() -> get('view') -> getEventManager();

        $em -> attach(Event::EVENT_RENDER, function(Event $e) use ($tagContent) {
                $view  = $e -> getTarget();
                $model = $view -> getModel();

                if (!$model instanceof ViewModel) {
                    return;
                }

                $model['content'] = $e -> getParam('__RESULT__');

                $result = $view -> getRenderer() -> render($tagContent, $model);

                $e -> setParam('__RESULT__', $result);
                return $result;
            }, 10);
    } // after()

}