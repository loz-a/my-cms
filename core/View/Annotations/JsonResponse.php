<?php
namespace Core\View\Annotations;

use Core\App;
use Core\View\Model\JsonModel;
use Core\View\ViewEvent as Event;

/**
 *  Annotation example:
 *  @Core.View.Annotations.JsonResponse
 */
class JsonResponse
{
    public function after()
    {
        $em = App::getInstance() -> getServiceManager() -> get('view') -> getEventManager();

        $em -> attach(Event::EVENT_RENDER, function(Event $e) {
            $view   = $e -> getTarget();
            $model  = $view -> getModel();

            if (!$model instanceof JsonModel) {
                return;
            }

            $result = $view -> getRenderer() -> render($model);

            $e -> setParam('__RESULT__', $result);
            return $result;
        }, 15);
    } // after()

}
