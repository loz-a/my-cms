<?php
namespace Core\View\Helper;

use Core\SingletonTrait;
use Core\View\Exception\InvalidArgumentException;
use Core\View\Exception\RuntimeException;
use Zend\Stdlib\SplPriorityQueue;

class CaptureContent
{
    use SingletonTrait;

    /**
     * Container for captured content
     * @var array
     */
    protected $container = [];

    /**
     * Whether or not we're already capturing for this given container
     * @var bool
     */
    protected $captureLock = false;

    /**
    /**
     * Key to which to capture content
     * @var string
     */
    protected $captureKey;

    /**
     * Priority to which to capture content
     * @var int
     */
    protected $capturePriority = 1;

    /**
     * @var string
     */
    protected $separator = PHP_EOL;


    /**
     * @param $key
     * @param $value
     * @param int $priority
     * @return $this|bool
     * @throws \Core\View\Exception\InvalidArgumentException
     */
    public function insert($key, $value, $priority = 1)
    {
        if (!$key or !is_string($key)) {
            throw new InvalidArgumentException('Invalid "key" argument');
        }

        if (!$value) {
            return false;
        }
        $value = (string) $value;

        if (!isset($this -> container[$key])) {
            $this -> container[$key] = new SplPriorityQueue();
        }
        $this -> container[$key] -> insert($value, $priority);

        return $this;
    } // insert()


    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        if (isset($this -> container[$key])) {
            unset($this -> container[$key]);
        }
        $this -> insert($key, $value);

        return $this;
    } // set()


    public function start($key, $priority = 1)
    {
        if ($this -> captureLock) {
            throw new RuntimeException('Cannot nest placeholder captures for the same placeholder');
        }
        $this -> captureLock = true;
        $this -> captureKey = (string) $key;
        $this -> capturePriority = (int) $priority;
        ob_start();
    } // start()


    public function end()
    {
        $data = ob_get_clean();
        $this -> captureLock = false;
        $this -> insert($this -> captureKey, $data, $this -> capturePriority);
    } // end()


    public function toString($key)
    {
        $tmp = [];

        if (!isset($this -> container[$key])) {
            return '';
        }

        $queue = $this -> container[$key];
        while($queue -> valid()) {
            $tmp[] = trim($queue -> current());
            $queue -> next();
        }

        return $this -> separator . implode($this -> separator, $tmp);
    } // toString()


    public function setSeparator($separator)
    {
        $this -> separator = (string) $separator;
        return $this;
    } // setSeparator()

} // Container