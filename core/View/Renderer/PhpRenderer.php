<?php
namespace Core\View\Renderer;

use Core\View\Exception\TemplateNotFoundException;
use Core\View\Exception\InvalidViewModelException;
use Core\View\Model\ViewModel;
use Core\View\Resolver\ResolverInterface;

class PhpRenderer implements RendererInterface
{
    /**
     * @var \SplPriorityQueue
     */
    protected $resolversQueue;


    public function __construct()
    {
        $this -> resolversQueue = new \SplPriorityQueue();
    } // __construct()


    /**
     * Add template resolve
     *
     * @param ResolverInterface $resolver
     * @param int $priority
     * @return $this
     */
    public function addResolver(ResolverInterface $resolver, $priority = 1)
    {
        $this -> resolversQueue -> insert($resolver, $priority);
        return $this;
    } // setResolver()


    /**
     * @param $name
     * @return bool
     */
    public function getTemplatePath($name)
    {
        $resolvers = clone $this -> resolversQueue;

        foreach ($resolvers as $resolver) {
            if ($path = $resolver -> resolve($name)) {
                return $path;
            }
        }
        return false;
    } // getResolver()


    /**
     * @param $nameOrModel
     * @param null $model
     * @return string
     * @throws \Core\View\Exception\TemplateNotFoundException
     * @throws \Core\View\Exception\InvalidViewModelException
     */
    public function render($nameOrModel, $model = null)
    {
        if ($nameOrModel instanceof ViewModel) {
            $model = $nameOrModel;
            $nameOrModel = $model -> getTemplateName();
        }

        $file = $this -> getTemplatePath($nameOrModel);
        $content = '';
        if ($file) {

            if ($model instanceof ViewModel) {
                $model = $model -> toArray();
            }

            if (!is_array($model)) {
                throw new InvalidViewModelException('Invalid viewModel');
            }

            extract($model);

            ob_start();
            include $file;
            $content = ob_get_contents();
            ob_end_clean();
        }

        if (!$content) {
            throw new TemplateNotFoundException(sprintf('Template by name "%s" not found', $nameOrModel));
        }
        return $content;
    } // renderer()

} // PhpRenderer