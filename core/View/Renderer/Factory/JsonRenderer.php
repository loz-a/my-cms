<?php
namespace Core\View\Renderer\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Core\View\Renderer\JsonRenderer as ViewJsonRenderer;

class JsonRenderer implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $renderer = new ViewJsonRenderer();
        $renderer -> setResponse($serviceLocator -> get('response'));
        return $renderer;
    } // createService()

} 