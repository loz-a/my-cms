<?php
namespace Core\View\Renderer\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Core\View\Renderer\PhpRenderer as ViewPhpRenderer;

class PhpRenderer implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $renderer = new ViewPhpRenderer();
        $renderer -> addResolver($serviceLocator -> get('view.map_resolver'), 10);
        $renderer -> addResolver($serviceLocator -> get('view.path_stack_resolver'), 5);

        return $renderer;
    } // create()
} 