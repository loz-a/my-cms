<?php
namespace Core\View;

use Zend\EventManager\Event as ZendEvent;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;

class ViewEvent extends ZendEvent
{
    const EVENT_RENDER = 'view.render';
    const EVENT_MODEL  = 'view.model.post';

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this -> request = $request;
        return $this;
    } // setRequest()


    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this -> request;
    } // getRequest()


    /**
     * @param Response $response
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this -> response = $response;
        return $this;
    } // setResponse()


    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this -> response;
    } // getResponse()

} // ViewEvent