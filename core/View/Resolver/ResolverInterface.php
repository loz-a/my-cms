<?php
namespace Core\View\Resolver;

/**
 * Base resolver interface
 *
 * @subpackage Resolver
 */
interface ResolverInterface
{
    /**
     * Resolve a template name
     *
     * @param  string $template
     * @return string|array
     */
    public function resolve($template);

}
