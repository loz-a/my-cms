<?php
namespace Core\View\Resolver;

use ArrayIterator;
use SplFileInfo;
use Core\View\Exception\InvalidArgumentException;

class TemplateMapResolver implements ResolverInterface
{
    /**
     * @var array
     */
    protected $map = array();


    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this -> map);
    } // getIterator()


    /**
     * @param string $name
     * @param string $path
     * @return $this
     * @throws \Core\View\Exception\InvalidArgumentException
     */
    public function addTemplatePath($name, $path)
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException(sprintf(
                '%s: expects a string for the first argument; received "%s"',
                __METHOD__,
                (is_object($name) ? get_class($name) : gettype($name))
            ));
        }

        if (empty($path)) {
            if (isset($this -> map[$name])) {
                unset($this -> map[$name]);
            }
            return $this;
        }

        $this -> map[$name] = $path;
        return $this;
    } // addPath()


    /**
     * @param array $templates
     * @return $this
     */
    public function addTemplatePaths(array $templates)
    {
        foreach ($templates as $name => $path) {
            $this -> addTemplatePath($name, $path);
        }
        return $this;
    } // addTemplatePaths()


    /**
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this -> map);
    } // has()


    /**
     * @param string $name
     * @return bool
     */
    public function get($name)
    {
        if (!$this -> has($name)) {
            return false;
        }
        return $this -> map[$name];
    } // get()


    /**
     * @return array
     */
    public function getMap()
    {
        return $this -> map;
    } // getMap()


    /**
     * Resolve a template name
     *
     * @param  string $name
     * @return string|array
     */
    public function resolve($name)
    {
        $template = $this -> get($name);

        if ($template) {
            $fileInfo = new SplFileInfo($template);
            if ($fileInfo -> isReadable()) {
                return $fileInfo -> getRealPath();
            }
        }
        return false;
    } // resolve()

}