<?php
namespace Core\Annotations\Action;

use Core\App;
use Core\Exception\InvalidTypeException;

/**
 *  Annotation example:
 *  single argument
 *  @Core.Annotations.Action.RouteMatch int id
 *
 *  lot of arguments:
 *  @Core.Annotations.Action.RouteMatch int id, str user, bool flag
 */
class RouteMatch
{

    public function before($tagContent)
    {
        $app = App::getInstance();
        $routeMatch = $app -> getServiceManager() -> get('route_match');
        $splittedTagContent = explode(',', $tagContent);

        foreach ($splittedTagContent as $pair) {
            list($type, $name) = explode(' ', trim($pair));
            $value = $routeMatch -> getParam($name);

            try {
                $value = cast_to_type($value, $type);
            }
            catch(InvalidTypeException $e) {
                throw new InvalidTypeException(sprintf('Invalid type "%s" for parameter "%s"', $type, $name));
            }
            $app -> getParams() -> set($name, $value);
        }
    } // before()

}