<?php
namespace Core\Annotations;


class Manager
{
    /**
     * @var array
     */
    protected $parsedDocBlocks = array();

    /**
     * @param string $action
     * @param array $dockBlock
     * @return $this
     */
    public function addDockBlock($action, $dockBlock)
    {
        $this -> parsedDocBlocks[$action] = is_null($dockBlock) ? null : $dockBlock;
        return $this;
    } // addDockBlock()


    /**
     * @param $action
     * @return null|array
     */
    public function getDocBlock($action)
    {
        return array_key_exists($action, $this -> parsedDocBlocks) ? $this -> parsedDocBlocks[$action] : null;
    } // getDocBlock()


    /**
     * @param $action
     * @return $this
     */
    public function removeDockBlock($action)
    {
        if (array_key_exists($action, $this -> parsedDocBlocks)) {
            unset($this -> parsedDocBlocks[$action]);
        }
        return $this;
    } // removeDockBlock()


    /**
     * @return $this
     */
    public function clearDockBlocks()
    {
        $this -> parsedDocBlocks = array();
        return $this;
    } // clearDockBlock()
}