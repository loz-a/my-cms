<?php
namespace Core\Annotations;

interface AnnotationsManagerAwareInterface
{
    /**
     * @param Manager $annotationsManager
     * @return $this
     */
    public function setAnnotationsManager(Manager $annotationsManager);

    /**
     * @return Manager
     */
    public function getAnnotationsManager();
}