<?php
namespace Core\App\View;

use Core\View\View;
use Core\View\ViewEvent;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements  FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        include_once getcwd() . '/core/View/functions.php';

        $config = $serviceLocator -> get('config') -> view;

        $serviceLocator -> get('view.path_stack_resolver') -> addTemplatePaths($config -> template_path_stack -> toArray());
        $serviceLocator -> get('view.map_resolver')        -> addTemplatePaths($config -> template_map -> toArray());

        $event = new ViewEvent();
        $event
            -> setRequest($serviceLocator -> get('request'))
            -> setResponse($serviceLocator -> get('response'));

        $view  = new View();
        $view -> setEvent($event);

        $view
            -> getEventManager()
            -> attach(ViewEvent::EVENT_MODEL, function($e) use ($serviceLocator) {
                $target = $e -> getTarget();

                $containerId = sprintf('view.%s_renderer', $target -> getModel() -> modelType());
                $target -> setRenderer($serviceLocator -> get($containerId));
            });

        $serviceLocator
            -> setFactory('view.widgets', function() use ($config, $view) {
                $config   = isset($config -> widgets_service_manager) ? $config -> widgets_service_manager : array();
                $smConfig = new ServiceManagerConfig($config -> toArray());
                return (new WidgetManager($smConfig)) -> setView($view);
            });

        return $view;
    } // createService()
} 