<?php
namespace Core\App;

use Zend\EventManager\EventManagerAwareTrait as BaseEventManagerAwareTrait;
use Zend\EventManager\Event;

trait EventManagerAwareTrait {

    use BaseEventManagerAwareTrait;

    /**
     * Trigger a named event
     *
     * Allows optionally passing more params if desired.
     *
     * @param  string $name
     * @param  array $params
     * @return \Zend\EventManager\ResponseCollection
     */
    public function trigger($name, $params = null)
    {
        $event = ($name instanceof Event) ? $name : new Event($name, $this, $params) ;
        $this -> getEventManager() -> trigger($event);
    } // trigger()

} 