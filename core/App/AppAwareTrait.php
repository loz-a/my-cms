<?php
namespace Core\App;

use Core\App;
use Core\App\Exception\ApplicationUndefinedException;

trait AppAwareTrait
{
    /**
     * @var App
     */
    protected $app;

    /**
     * @param \Core\App $app
     * @return $this
     */
    public function setApplication(App $app)
    {
        $this -> app = $app;
        return $this;
    }


    /**
     * @return \Core\App
     * @throws ApplicationUndefinedException
     */
    public function getApplication()
    {
        if (null === $this -> app) {
            throw new ApplicationUndefinedException('Application is undefined');
        }
        return $this -> app;
    }


    /**
     * @param $name
     * @return array|object
     */
    public function get($name)
    {
        return $this -> getApplication() -> getServiceManager() -> get($name);
    } // get()
} 