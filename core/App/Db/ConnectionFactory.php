<?php
namespace Core\App\Db;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PDO;

class ConnectionFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbConfig = $serviceLocator -> get('config') -> db;
        
        $dsn      = $dbConfig -> dsn;
        $user     = $dbConfig -> user;
        $password = $dbConfig -> password;
        
        $dbh = new PDO($dsn, $user, $password);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if ($dbh -> getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
            $dbh -> setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
        }

        return $dbh;
    }

} 