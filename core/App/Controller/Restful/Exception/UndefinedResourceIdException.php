<?php
namespace Core\App\Controller\Restful\Exception;

class UndefinedResourceIdException extends \Exception
    implements ExceptionInterface
{
}