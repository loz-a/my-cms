<?php
namespace Core\App\Controller;

use Core\App;

interface ControllerInterface
{
    public function callAction(App $application);
} 