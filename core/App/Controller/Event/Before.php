<?php
namespace Core\App\Controller\Event;

use Core\Annotations\AnnotationsManagerAwareInterface;
use Zend\EventManager\Event;

class Before extends Event
{
    const EVENT_NAME = 'call.controller.pre';
    /**
     * @var AnnotationsManagerAwareInterface
     */
    protected $controller;

    /**
     * @var string
     */
    protected $actionName;


    public function __construct($target = null, $controller = null, $actionName = null, $params = null)
    {
        parent::__construct(static::EVENT_NAME, $target, $params);

        if (null !== $controller) {
            $this -> setController($controller);
        }

        if (null !== $actionName) {
            $this -> setActionName($actionName);
        }
    }

    /**
     * @param string $actionName
     * @return $this
     */
    public function setActionName($actionName)
    {
        $this -> actionName = (string) $actionName;
        return $this;
    }


    /**
     * @return string
     */
    public function getActionName()
    {
        return $this -> actionName;
    }


    /**
     * @param AnnotationsManagerAwareInterface $controller
     * @return $this
     */
    public function setController(AnnotationsManagerAwareInterface $controller)
    {
        $this -> controller = $controller;
        return $this;
    }

    /**
     * @return AnnotationsManagerAwareInterface
     */
    public function getController()
    {
        return $this -> controller;
    }

} 