<?php
namespace Core\App\Controller\Event;

class After extends Before
{
    const EVENT_NAME = 'call.controller.post';

    protected $result;

    public function __construct($target = null, $controllerName = null, $actionName = null, $result = null, $params = null)
    {
        parent::__construct($target, $controllerName, $actionName, $params);

        if (null !== $result) {
            $this -> setResult($result);
        }
    }


    /**
     * @param mixed $result
     * @return $this
     */
    public function setResult($result)
    {
        $this -> result = $result;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this -> result;
    }


} 