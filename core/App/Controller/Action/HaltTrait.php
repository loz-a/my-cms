<?php
namespace Core\App\Controller\Action;

use Core\App\Exception\HaltException;

trait HaltTrait
{
    /**
     * Halt execution
     *
     * Halts execution, and sets the response status code to $status, as well
     * as sets the response body to the provided message (if any). Any previous
     * content in the response body will be overwritten.
     *
     * @param  int    $status  HTTP response status
     * @param  string $message HTTP response body
     * @return void
     * @throws HaltException
     */
    public function halt($status, $message = '')
    {
        $response = $this -> getApplication() -> getServiceManager() -> get('response');
        $response -> setStatusCode($status);
        $response -> setContent($message);

        throw new HaltException();
    } // halt()


    /**
     * @return \Core\App
     */
    abstract public function getApplication();
} 