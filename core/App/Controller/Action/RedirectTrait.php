<?php
namespace Core\App\Controller\Action;

use Core\App\Exception\HaltException;
use Zend\Uri\UriInterface;

trait RedirectTrait
{
    public function redirect($uri, $status = 302)
    {
        if ($uri instanceof UriInterface) {
            $uri = $uri -> toString();
        }

        $response = $this -> getApplication() -> getServiceManager() -> get('response');
        $response -> getHeaders() -> addHeaderLine('Location', $uri);
        $response -> setStatusCode($status);

        throw new HaltException();
    } // redirect()


    /**
     * @return \Core\App
     */
    abstract public function getApplication();
} 