<?php
namespace Core\App\Controller\Action;

trait FlashMessengerTrait
{
    /**
     * @var \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    protected $flashMessenger;

    /**
     * @return array|object|\Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    public function getFlashMessenger()
    {
        if (null === $this -> flashMessenger) {
            $this -> flashMessenger = $this -> getApplication() -> getServiceManager() -> get('flashMessenger');
        }
        return $this -> flashMessenger;
    } // getFlashMessenger()


    /**
     * @param $message
     * @return \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    public function messageInfo($message)
    {        
        return $this -> getFlashMessenger() -> addInfoMessage($message);
    } // messageInfo()


    /**
     * @param $message
     * @return \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    public function messageSuccess($message)
    {        
        return $this -> getFlashMessenger() -> addSuccessMessage($message);
    } // messageSuccess()


    /**
     * @param $message
     * @return \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    public function messageWarning($message)
    {
        return $this -> getFlashMessenger() -> addWarningMessage($message);
    } // messageWarning()


    /**
     * @param $message
     * @return \Zend\Mvc\Controller\Plugin\FlashMessenger
     */
    public function messageError($message)
    {
        return $this -> getFlashMessenger() -> addErrorMessage($message);
    } // messageError()


    /**
     * @return \Core\App
     */
    abstract public function getApplication();
} 