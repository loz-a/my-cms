<?php
namespace Core\App\Controller\Action;


use Core\App\Exception\PassException;

trait PassTrait {

    /**
     * Pass execution on to next matching route
     *
     * @throws PassException
     */
    public function pass()
    {
        throw new PassException();
    } // pass()

} 