<?php
namespace Core\App;

use Core\App;

interface AppAwareInterface
{
    /**
     * @param App $app
     * @return mixed
     */
    public function setApplication(App $app);

    /**
     * @return App
     */
    public function getApplication();

    /**
     * @param $name
     * @return array|object
     */
    public function get($name);
} 