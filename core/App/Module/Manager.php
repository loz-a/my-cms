<?php
namespace Core\App\Module;

use Core\App\AppAwareTrait;
use Core\App\EventManagerAwareTrait;
use Core\App as Application;
use Core\App\Exception\ModulesConfigurationUndefinedException;
use Zend\Config\Config;
use Zend\ServiceManager\Config as ServiceManagerConfig;
use Traversable;

class Manager
{
    use EventManagerAwareTrait, AppAwareTrait;

    /**
     * @var Config
     */
    protected $modulesConfig;

    public function __construct(Config $modulesConfig, Application $app)
    {
        $this -> setModulesConfig($modulesConfig);
        $this -> setApplication($app);
        $this -> registerModuleListener();
    } // __construct()


    public function setModulesConfig(Config $modulesConfig)
    {
        $this -> modulesConfig = $modulesConfig;
        return $this;
    } // setConfig()


    public function getModulesConfig()
    {
        if (null === $this -> modulesConfig or !$this -> modulesConfig instanceof Traversable) {
            throw new ModulesConfigurationUndefinedException('Modules configuration is undefined');
        }
        return $this -> modulesConfig;
    } // getConfig()


    public function loadModules()
    {
        $event  = new Event(null, $this);
        $modulesConfig = $this -> getModulesConfig();

        foreach($modulesConfig as $name => $path) {
            $event
                -> setModuleName($name)
                -> setModulePath($path)
                -> setParams(array());

            $this -> getEventManager() -> trigger('module.load', $event);
        }
        $this -> getApplication() -> trigger('modules.loaded');
    } // loadModules()


    protected function registerModuleListener()
    {
        $appConfig = $this -> getApplication() -> getServiceManager() -> get('config');

        $closure = function(Event $e) use ($appConfig) {
            $app = $e -> getTarget() -> getApplication();
            $modulePath = $e -> getModulePath();

            $file = sprintf('%s%sconfig.php', $modulePath, DIRECTORY_SEPARATOR);
            if (file_exists($file)) {
                $config = include $file;

                if (is_array($config)) {
                     $appConfig -> merge(new Config($config));

                    if (isset($config['service_manager'])) {
                        (new ServiceManagerConfig($config['service_manager'])) -> configureServiceManager($app -> getServiceManager());
                    }
                }
            }

            $file = sprintf('%s%sInit.php', $modulePath, DIRECTORY_SEPARATOR);
            if (file_exists($file)) {
                include_once $file;
                $Initializer = sprintf('%s\Init', ucfirst($e -> getModuleName()));
                if (class_exists($Initializer)) {
                    (new $Initializer()) -> __invoke($app);
                }
            }

            $file = sprintf('%s%sfunctions.php', $modulePath, DIRECTORY_SEPARATOR);
            if (file_exists($file)) {
                include_once $file;
            }
        };

        $this -> getEventManager() -> attach('module.load', $closure);
    } // registerModuleListener()
} 