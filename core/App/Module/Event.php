<?php
namespace Core\App\Module;

use Zend\EventManager\Event as BaseEvent;

class Event extends BaseEvent
{
    protected $moduleName;

    protected $modulePath;

    /**
     * @param mixed $moduleName
     * @return $this
     */
    public function setModuleName($moduleName)
    {
        $this -> moduleName = $moduleName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuleName()
    {
        return $this -> moduleName;
    }

    /**
     * @param mixed $modulePath
     * @return $this
     */
    public function setModulePath($modulePath)
    {
        $this -> modulePath = $modulePath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModulePath()
    {
        return $this -> modulePath;
    }
} 