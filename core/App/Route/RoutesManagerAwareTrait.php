<?php
namespace Core\App\Route;


trait RoutesManagerAwareTrait
{
    /**
     * @var Manager
     */
    protected $routesManager;

    /**
     * @param \Core\App\Route\Manager $routesManager
     * @return $this
     */
    public function setRoutesManager(Manager $routesManager)
    {
        $this -> routesManager = $routesManager;
        return $this;
    }


    /**
     * @return \Core\App\Route\Manager
     */
    public function getRoutesManager()
    {
        return $this -> routesManager;
    }


} 