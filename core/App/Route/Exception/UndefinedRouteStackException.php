<?php
namespace Core\App\Route\Exception;

use Core\Exception\ExceptionInterface;

class UndefinedRouteStackException extends \Exception implements ExceptionInterface
{

} 