<?php
namespace Core\App\Route;

interface RoutesManagerCapableInterface
{
    public function getRoutesManager();
} 