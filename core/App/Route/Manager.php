<?php
namespace Core\App\Route;

use Core\App\AppAwareTrait;
use Core\App as Application;
use Core\App\Exception\PageNotFoundException;
use Core\App\Route\Exception\UndefinedRouteStackException;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack;
use Zend\Http\PhpEnvironment\Request;

class Manager
{
    use AppAwareTrait;

    /**
     * @var TreeRouteStack
     */
    protected $routeStack;

    /**
     * Whether or not we've already registered the route listener
     *
     * @var bool
     */
    protected $routeListenerRegistered = false;


    /**
     * @param \Core\App $app
     */
    public function __construct(Application $app)
    {
        $this -> setApplication($app);
        $this -> registerAggregateRoutesListener();
    } // __construct()


    /**
     * @return TreeRouteStack
     * @throws Exception\UndefinedRouteStackException
     */
    public function getRouteStack()
    {
        if (null === $this -> routeStack) {
            throw new UndefinedRouteStackException('Route stack is undefined');
        }
        return $this -> routeStack;
    } // getRouteStack()


    /**
     * @param Request $request
     * @return mixed
     */
    public function route(Request $request)
    {
        $this -> registerRouteListener();
        $app    = $this -> getApplication();

        $result = $app -> getEventManager() -> trigger('route', $app, compact('request'), function($v) {
            return ($v instanceof RouteMatch);
        });

        if ($result -> stopped()) {
            return $result -> last();
        }
    } // route()


    protected function registerAggregateRoutesListener()
    {
        $closure = function() {
            $applicationConfig = $this -> getApplication() -> getServiceManager() -> get('config');
            $routesConfig = $applicationConfig -> get('routes', array());
            $this -> routeStack = TreeRouteStack::factory(array('routes' => $routesConfig -> toArray()));
        };

        $closure = $closure -> bindTo($this);
        $this -> getApplication() -> getEventManager() -> attach('modules.loaded', $closure);
    } // registerAggregateRoutesListener()


    protected function registerRouteListener()
    {
        if ($this -> routeListenerRegistered) {
            return;
        }

        $closure = function($e) {
            $request = $e -> getParam('request');

            $routeMatch  = $this -> getRouteStack() -> match($request);

            if ($routeMatch) {
                $this -> getApplication() -> getServiceManager() -> setService('route_match', $routeMatch);
                return $routeMatch;
            }
            throw new PageNotFoundException();

        };
        $closure = $closure -> bindTo($this);

        $this -> getApplication() -> getEventManager() -> attach('route', $closure);
        $this -> routeListenerRegistered = true;
    } // registerRouteListener


    /**
     * Generates a URL based on a named route
     *
     * @param  string $route Named Route instance
     * @param  array $params Parameters to use in url generation, if any
     * @param  array $options
     * @return string
     */
    public function urlFor($route = null, array $params = array(), array $options = array())
    {
        if (null === $route) {
            $route = $this -> getApplication() -> getServiceManager() -> get('route_match') -> getMatchedRouteName();
        }
        $options['name'] = $route;
        return $this -> getRouteStack() -> assemble($params, $options);
    } // urlFor()

} 