<?php
namespace Core\App\Exception;

use Core\Exception\ExceptionInterface;
/**
 * Exception indicating a 404 condition
 */
class PageNotFoundException extends \Exception implements ExceptionInterface
{}
