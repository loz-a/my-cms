<?php
namespace Core\App\Exception;

use Core\Exception\ExceptionInterface;

/**
 * Exception indicating a halt condition
 */
class HaltException extends \Exception implements ExceptionInterface
{}
