<?php
namespace Core\App\Exception;

use Core\Exception\ExceptionInterface;
/**
 * Exception indicating an invalid controller
 */
class InvalidControllerException extends \DomainException implements ExceptionInterface
{}
