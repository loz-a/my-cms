<?php
namespace Core\App\Exception;

use Core\Exception\ExceptionInterface;

class ApplicationUndefinedException extends \Exception implements ExceptionInterface
{

} 