<?php
namespace Core\App\Exception;

use Core\Exception\ExceptionInterface;
/**
 * Exception indicating we should pass on to next matching route
 */
class PassException extends \Exception implements ExceptionInterface
{}
