<?php
namespace Core\App;

use Core\Stdlib\Params;
use Zend\Json\Json;
use Zend\Http\Request;

class RequestParams extends Params
{
    const CONTENT_TYPE_JSON = 'json';

    /**
     * @var array
     */
    protected $contentTypes = array(
        self::CONTENT_TYPE_JSON => array(
            'application/hal+json',
            'application/json'
        )
    );


    public function __construct(Request $request)
    {
        $method = strtolower($request -> getMethod());

        switch ($method) {
            case 'post':
                $this -> set($this -> getPostData($request));
                break;

            case 'put':
            case 'patch':
                $this -> set($this -> processBodyContent($request));
                break;

            default:
                // get, delete, head
                $this -> set($request -> getQuery() -> toArray());
        }
    } // __construct()


    /**
     * Check if request has certain content type
     *
     * @param  Request $request
     * @param  string|null $contentType
     * @return boolean
     */
    public function requestHasContentType(Request $request, $contentType = '')
    {
        $headerContentType = $request -> getHeaders() -> get('content-type');
        if (!$headerContentType) {
            return false;
        }

        $requestedContentType = $headerContentType -> getFieldValue();
        if (strstr($requestedContentType, ';')) {
            $headerData = explode(';', $requestedContentType);
            $requestedContentType = array_shift($headerData);
        }

        $requestedContentType = trim($requestedContentType);
        if (array_key_exists($contentType, $this -> contentTypes)) {
            foreach ($this -> contentTypes[$contentType] as $contentTypeValue) {
                if (stripos($contentTypeValue, $requestedContentType) === 0) {
                    return true;
                }
            }
        }
        return false;
    } // requestHasContentType()


    /**
     * @param Request $request
     * @return array
     */
    public function getPostData(Request $request)
    {
        return ($this -> requestHasContentType($request, self::CONTENT_TYPE_JSON))
            ? Json::decode($request->getContent(), Json::TYPE_ARRAY)
            : $request -> getPost() -> toArray();
    } // getPostData()


    /**
     * Process the raw body content
     *
     * If the content-type indicates a JSON payload, the payload is immediately
     * decoded and the data returned. Otherwise, the data is passed to
     * parse_str(). If that function returns a single-member array with a key
     * of "0", the method assumes that we have non-urlencoded content and
     * returns the raw content; otherwise, the array created is returned.
     *
     * @param  mixed $request
     * @return object|string|array
     */
    protected function processBodyContent($request)
    {
        $content = $request -> getContent();

        // JSON content? decode and return it.
        if ($this -> requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            return Json::decode($content, Json::TYPE_ARRAY);
        }

        parse_str($content, $parsedParams);

        // If parse_str fails to decode, or we have a single element with key
        // 0, return the raw content.
        if (!is_array($parsedParams)
            || (1 == count($parsedParams) && isset($parsedParams[0]))
        ) {
            return $content;
        }

        return $parsedParams;
    } // processBodyContent()
} 