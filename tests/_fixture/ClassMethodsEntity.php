<?php
use Core\Stdlib\Entity\ClassMethods;

class ClassMethodsEntity extends ClassMethods
{
    protected $title;

    protected $text;

    public function setText($text)
    {
        $this -> text = $text;
        return $this;
    }

    public function getText()
    {
        return $this -> text;
    }
} 