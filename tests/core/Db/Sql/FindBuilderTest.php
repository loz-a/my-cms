<?php
namespace Core\Db\Sql;


class FindBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCrreateInstance()
    {
        $instance = new FindBuilder();
        $this -> isInstanceOf('Core\Db\Sql\FindBuilder', $instance);
    } // testCanCrreateInstance()


    public function testFindBy()
    {
        $finder = new FindBuilder();
        $finder -> setTableName('pages');
        
        $result = $finder -> findByAliasAndTitle('test_alias', 'test_title');
        
        $this -> assertEquals('select * from pages where alias = :alias and title = :title', $result[0]);
        $this -> assertEquals(array(':alias' => 'test_alias', ':title' => 'test_title'), $result[1]);
    } // testFindBy()


    public function testFindOneBy()
    {
        $finder = new FindBuilder();
        $finder -> setTableName('pages');
        
        $result = $finder -> findOneByAliasAndTitle('test_alias', 'test_title');
        
        $this -> assertEquals('select * from pages where alias = :alias and title = :title limit 1', $result[0]);
        $this -> assertEquals(array(':alias' => 'test_alias', ':title' => 'test_title'), $result[1]);
    } // testFindOneBy()


    public function testInvalidArgumentException()
    {
        $this -> setExpectedException('Core\Db\Sql\Exception\InvalidArgumentException');

        $finder = new FindBuilder();
        $finder -> setTableName('pages') -> findByTitle();
    } // testInvalidArgumentException()


    public function testFakeMethodException()
    {
        $this -> setExpectedException('Core\Db\Sql\Exception\BadMethodCallException');
        $finder = new FindBuilder();
        $finder -> setTableName('pages') -> findBy();
    } // testBadMethodCallException()


    public function testSetGetTable()
    {
        $finder = new FindBuilder();
        $finder -> setTableName('pages');
        $this -> assertEquals('pages', $finder -> getTableName());
    } // testSetGetTable()


    public function testUndefinedTableException()
    {
        $this -> setExpectedException('Core\Db\Sql\Exception\InvalidClauseException');
        $finder = new FindBuilder();
        $finder -> getTableName();
    } // testUndefinedTableException()
} 