<?php
namespace Core\Db\Sql;

use Core\Db\Sql\QueryBuilder\From\SubqueryBuilder as FromSubqueryBuilder;
use Faker;
use Core\Db\Sql\Params\Params;


class QueryBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()

    public function testCanCreateInstance()
    {
        $qb = new QueryBuilder();
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder', $qb);
    } // testCanCreateInstance()


    public function testSelect()
    {
        $qb = new QueryBuilder();
        $qb -> select() -> columns(array('alias', 'title', 'page_content' => 'content'));
        $this -> assertEquals('select alias, title, content as page_content', $qb -> build()[0]);
    } // testSelect()


    public function testSelectArgumentsAsArray()
    {
        $qb = new QueryBuilder();
        $qb -> select(array('alias', 'title', 'page_content' => 'content'));
        $this -> assertEquals('select alias, title, content as page_content', $qb -> build()[0]);
    } // testSelectArgumentsAsArray()


    public function testSelectArgumentsWithoutArray()
    {
        $qb = new QueryBuilder();
        $qb -> select('alias', 'title', 'content');
        $this -> assertEquals('select alias, title, content', $qb -> build()[0]);
    } // testSelectArgumentsWithoutArray()


    public function testSelectDefault()
    {
        $qb = new QueryBuilder();
        $qb -> select();
        $this -> assertEquals('select *', $qb -> build()[0]);
    } // testSelectDefault()


    public function testFrom()
    {
        $qb = new QueryBuilder();
        $qb
            -> select() -> columns(array('alias', 'title', 'content')) -> qb()
            -> from('pages', 'pages_with_content');
        $this -> assertEquals('select alias, title, content from pages as pages_with_content', $qb -> build()[0]);
    } // testFrom()


    public function testFromWithoutSelect()
    {
        $qb = new QueryBuilder();
        $qb -> from('pages', 'pages_with_content');
        $this -> assertEquals('select * from pages as pages_with_content', $qb -> build()[0]);
    } // testFromWithoutSelect()


    public function testFromSubquery()
    {
        $qb = new QueryBuilder();
        $qb -> from(
            (new FromSubqueryBuilder())
                -> select('type', 'model', 'speed')
                -> from('products')
                -> where() -> gt('speed', 600)
                -> group() -> by('alias') -> lt('date_create', 999999999)
        );

        $result = $qb -> build();

        $this -> assertEquals('select * from (select type, model, speed from products where speed > :where_from_0 group by alias having date_create < :group_from_1)', $result[0]);
        $this -> assertEquals(array(':where_from_0' => 600, ':group_from_1' => 999999999), $result[1]);
    } // testFromSubquery()


    public function testWhere()
    {
        $time = Faker\Factory::create() -> unixTime();
        $qb   = new QueryBuilder();
        $qb
            -> select() -> columns('alias')
            -> from('pages')
            -> where() -> lt('date_create', $time) -> andIsNotNull('views');

        $qbResult = $qb -> build();
        $this -> assertEquals('select alias from pages where date_create < :where_0 and views is not null', $qbResult[0]);
        $this -> assertEquals(array(':where_0' => $time), $qbResult[1]);
    } // testWhere()


    public function testJoin()
    {
        $time = Faker\Factory::create() -> unixTime();
        $qb   = new QueryBuilder();
        $qb
            -> select() -> columns('pages.alias')
            -> from('pages', 'p')
            -> join() -> with('pages_translations', 't') -> on('p.id = t.page_id') -> andGte('p.create_date', $time) -> qb()
            -> where() -> lt('p.date_create', $time) -> andIsNotNull('p.views');

        $qbResult = $qb -> build();
        $this -> assertEquals('select pages.alias from pages as p inner join pages_translations as t on p.id = t.page_id and p.create_date >= :join_0 where p.date_create < :where_1 and p.views is not null', $qbResult[0]);
        $this -> assertEquals(array(':where_1' => $time, ':join_0' => $time), $qbResult[1]);
    } // testJoin()


    public function testJoinArguments()
    {
        $time = Faker\Factory::create() -> unixTime();
        $qb   = new QueryBuilder();
        $qb
            -> select() -> columns('pages.alias')
            -> from('pages', 'p')
            -> join('pages_translations', 't') -> on('p.id = t.page_id') -> andGte('p.create_date', $time) -> qb()
            -> where() -> lt('p.date_create', $time) -> andIsNotNull('p.views');

        $qbResult = $qb -> build();
        $this -> assertEquals('select pages.alias from pages as p inner join pages_translations as t on p.id = t.page_id and p.create_date >= :join_0 where p.date_create < :where_1 and p.views is not null', $qbResult[0]);
        $this -> assertEquals(array(':where_1' => $time, ':join_0' => $time), $qbResult[1]);
    } // testJoinArguments()


    public function testMultiJoinsWithArguments()
    {
        $qb = new QueryBuilder();
        $qb
            -> select(
                'p.id', 't.id as translation_id', 't.title', 'pc.content as preparsed_content'
            )
            -> from('pages', 'p')
            -> join() -> left('pages_translations', 't')
                -> on('p.id = t.page_id')
                -> andEqual('t.locale_id', 1)
            -> join() -> left('pages_preparsed_content', 'pc')
                -> on('t.id = pc.id')
                -> andGt('t.id', 5)
            -> limit(1);

        $qbResult = $qb -> build();
        $this -> assertEquals('select p.id, t.id as translation_id, t.title, pc.content as preparsed_content from pages as p left join pages_translations as t on p.id = t.page_id and t.locale_id = :join_0 left join pages_preparsed_content as pc on t.id = pc.id and t.id > :join_1 limit 1',$qbResult[0]);
        $this -> assertEquals(array(':join_0' => 1, ':join_1' => 5), $qbResult[1]);
    } // testMultiJoinsWithArguments()


    public function testGroup()
    {
        $time = Faker\Factory::create() -> unixTime();
        $qb = new QueryBuilder();
        $qb -> select() -> columns(array('page_alias' => 'alias', 'page_title' => 'title'))
            -> from('pages')
            -> group() -> by(array('alias', 'date_create')) -> lte('date_create', $time);

        $qbResult = $qb -> build();
        $this -> assertEquals('select alias as page_alias, title as page_title from pages group by alias, date_create having date_create <= :group_0', $qbResult[0]);
        $this -> assertEquals(array(':group_0' => $time), $qbResult[1]);
    } // testGroup()


    public function testGroupArguments()
    {
        $time = Faker\Factory::create() -> unixTime();
        $qb = new QueryBuilder();
        $qb -> select() -> columns(array('page_alias' => 'alias', 'page_title' => 'title'))
            -> from('pages')
            -> group('alias', 'date_create') -> lte('date_create', $time);

        $qbResult = $qb -> build();
        $this -> assertEquals('select alias as page_alias, title as page_title from pages group by alias, date_create having date_create <= :group_0', $qbResult[0]);
        $this -> assertEquals(array(':group_0' => $time), $qbResult[1]);
    } // testGroupArguments()


    public function testOrder()
    {
        $qb = new QueryBuilder();
        $qb -> select() -> columns('alias')
            -> from('pages')
            -> order() -> desc('date_create');

        $this -> assertEquals('select alias from pages order by date_create desc', $qb -> build()[0]);
    } // testOrder()


    public function testLimit()
    {
        $qb = new QueryBuilder();
        $qb -> select() -> columns('alias')
            -> from('pages')
            -> limit(20, 10);

        $this -> assertEquals('select alias from pages limit 10, 20', $qb -> build()[0]);
    } // testLimit()


    public function testAllQueryBuilderStatements()
    {
        $time = Faker\Factory::create() -> unixTime();
        $qb   = new QueryBuilder();
        $qb
            -> select() -> columns('pages.alias')
            -> from('pages', 'p')
            -> join() -> with('pages_translations', 't') -> on('p.id = t.page_id') -> andGte('p.create_date', $time)
            -> where() -> lt('p.date_create', $time) -> andIsNotNull('p.views')
            -> group() -> by(array('p.alias', 'p.date_create')) -> lte('p.date_create', $time)
            -> order() -> desc('p.date_create')
            -> limit(20, 10);

        $qbResult = $qb -> build();
        $this -> assertEquals('select pages.alias from pages as p inner join pages_translations as t on p.id = t.page_id and p.create_date >= :join_0 where p.date_create < :where_1 and p.views is not null group by p.alias, p.date_create having p.date_create <= :group_2 order by p.date_create desc limit 10, 20', $qbResult[0]);
        $this -> assertEquals(array(':where_1' => $time, ':join_0' => $time, ':group_2' => $time), $qbResult[1]);
    } // testAllQueryBuilderStatements()
}
