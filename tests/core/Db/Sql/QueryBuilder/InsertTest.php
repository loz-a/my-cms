<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Sql\Params\Params;

class InsertTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()

    public function testCanCreateInstance()
    {
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Insert', new Insert());
    } // testCanCreateInstance()


    public function testColumnsAndValues()
    {
        $instance = new Insert();
        $instance
            -> into('product')
            -> columns('model', 'maker')
            -> values('test_model', 'test_maker');

        $result = $instance -> build();

        $this -> assertEquals('insert into product (model, maker) values (?,?)', $result[0]);
        $this -> assertEquals(array('test_model', 'test_maker'), $result[1]);
    } // testColumnsAndValues()


    public function testValues()
    {
        $instance = new Insert();
        $instance
            -> into('product')
            -> values(array('test_model', 'test_maker'));

        $result = $instance -> build();

        $this -> assertEquals('insert into product values (?,?)', $result[0]);
        $this -> assertEquals(array('test_model', 'test_maker'), $result[1]);
    } // testValues()


    public function testDefaultValues()
    {
        $instance = new Insert();
        $instance -> into('product');

        $result = $instance -> build();

        $this -> assertEquals('insert into product default values', $result[0]);
        $this -> assertEmpty($result[1]);
    } // testDefaultValues()


    public function testSubqueryWithInsertColumns()
    {
        $instance = new Insert();
        $instance
            -> into('product')
            -> columns(array('maker', 'model', 'type'))
            -> values(
                (new QueryBuilder())
                -> select() -> columns(array('maker', 'model', 'type'))
                -> from('catalog')
                -> where() -> equal('type', 'pc')
            );

        $result = $instance -> build();

        $this -> assertEquals('insert into product (maker, model, type) select maker, model, type from catalog where type = :where_0', $result[0]);
        $this -> assertEquals(array(':where_0' => 'pc'), $result[1]);
    } // testSubqueryWithInsertColumns()


    public function testSubqueryWithoutInsertColumns()
    {
        $instance = new Insert();
        $instance
            -> into('product')
            -> values(
                (new QueryBuilder())
                    -> select() -> columns(array('maker', 'model', 'type'))
                    -> from('catalog')
                    -> where() -> equal('type', 'pc')
            );

        $result = $instance -> build();

        $this -> assertEquals('insert into product select maker, model, type from catalog where type = :where_0', $result[0]);
        $this -> assertEquals(array(':where_0' => 'pc'), $result[1]);
    } // testSubqueryWithoutInsertColumns()


    public function testSetTableViaConstructor()
    {
        $result = (new Insert('product')) -> build();
        $this -> assertEquals('insert into product default values', $result[0]);
    } // testSetTableViaConstructor()


    public function testColumnsAndValuesAsArray()
    {
        $result = (new Insert('products'))
            -> columns(array('maker', 'model', 'type'))
            -> values(array('test_maker', 'test_model', 'test_type'))
            -> build();

        $this -> assertEquals('insert into products (maker, model, type) values (?,?,?)', $result[0]);
        $this -> assertEquals(array('test_maker', 'test_model', 'test_type'), $result[1]);
    } // testColumnsAndValuesAsArray()


    public function testColumnsAndValuesAsNotArray()
    {
        $result = (new Insert('products'))
            -> columns('maker', 'model', 'type')
            -> values('test_maker', 'test_model', 'test_type')
            -> build();

        $this -> assertEquals('insert into products (maker, model, type) values (?,?,?)', $result[0]);
        $this -> assertEquals(array('test_maker', 'test_model', 'test_type'), $result[1]);
    } // testColumnsAndValuesAsNotArray()


    public function testUndefinedTableException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Insert();
        $instance -> build();
    } // testUndefinedTableException()


    public function testColumnsNotEqualValuesException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Insert('product');
        $instance -> columns('maker', 'model', 'type') -> values('test_maker') -> build();
    } // testColumnsNotEqualValuesException()
} 