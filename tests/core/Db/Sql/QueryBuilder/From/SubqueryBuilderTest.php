<?php
namespace Core\Db\Sql\QueryBuilder\From;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Sql\Params\Params;

class SubqueryBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()

    public function testCanCreateInstance()
    {
        $qb = new SubqueryBuilder();
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder', $qb);
    } // testCanCreateInstance()


    public function testSubqueryWithDifferentSubqueryPlaceholders()
    {
        $qb = new QueryBuilder();
        $qb -> from(
            (new SubqueryBuilder())
                -> select('type', 'model', 'speed')
                -> from('products')
                -> where() -> gt('speed', 600)
                -> group() -> by('alias') -> lt('date_create', 999999999)
        );

        $result = $qb -> build();

        $this -> assertEquals('select * from (select type, model, speed from products where speed > :where_from_0 group by alias having date_create < :group_from_1)', $result[0]);
        $this -> assertEquals(array(':where_from_0' => 600, ':group_from_1' => 999999999), $result[1]);
    } // testSubqueryWithDifferentSubqueryPlaceholders()


    public function testSubqueryWithDifferentQueryAbdSubqueryPlaceholders()
    {
        $qb = new QueryBuilder();
        $qb -> from(
            (new SubqueryBuilder())
                -> select('type', 'model', 'speed')
                -> from('products')
                -> where() -> gt('speed', 600)
                -> group() -> by('alias') -> lt('date_create', 999999999)
        )
        -> where() -> gt('speed', 600)
        -> group() -> by('alias') -> lt('date_create', 999999999);

        $result = $qb -> build();

        $this -> assertEquals('select * from (select type, model, speed from products where speed > :where_from_0 group by alias having date_create < :group_from_1) where speed > :where_2 group by alias having date_create < :group_3', $result[0]);
        $this -> assertEquals(array(':where_from_0' => 600, ':group_from_1' => 999999999, ':where_2' => 600, ':group_3' => 999999999), $result[1]);
    } // testSubqueryWithDifferentQueryAbdSubqueryPlaceholders()
} 