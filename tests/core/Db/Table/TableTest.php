<?php
namespace Core\Db\Table;

use Core\Db\Sql\QueryBuilder;
use PDO;
use Faker;


class DbTableTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Table
     */
    protected $table;

    public function setUp()
    {
        $dbh = new PDO('mysql:host=localhost;dbname=my_cms_test', 'root', 'AM23naRmysql');
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this -> table = new Table();
        $this -> table
            -> setConnection($dbh)
            -> setTableName('pages');

        $dbh -> exec('alter table pages auto_increment=1');
        $this -> table -> getConnection() -> beginTransaction();
    } // setUp()


    public function tearDown()
    {
        $this -> table -> getConnection() -> rollBack();
    } // tearDown()


    protected function generateData($rowCount = 3)
    {
        $faker = Faker\Factory::create();
        $data   = array();

        if ($rowCount === 1) {
            $data['title']       = $faker -> sentence();
            $data['alias']       = str_replace(' ', '_', strtolower($data['title']));
            $data['content']     = $faker -> text();
            $data['date_create'] = $faker -> unixTime();
        } else {
            for($i = 0; $i < $rowCount; $i++) {
                $data[$i]['title']       = $faker -> sentence();
                $data[$i]['alias']       = str_replace(' ', '_', strtolower($data[$i]['title']));
                $data[$i]['content']     = $faker -> text();
                $data[$i]['date_create'] = $faker -> unixTime();
            }
        }
        return $data;
    } // generateData()


    protected function dbMultiInsert(array $data)
    {
        $sql = sprintf('insert into pages (title, alias, content, date_create) values(:%s)', join(', :', array_keys($data[0])));
        $sth = $this -> table -> getConnection() -> prepare($sql);
        
        for ($i = 0; $i < count($data); $i++) {
            $sth -> bindParam(':title', $data[$i]['title']);
            $sth -> bindParam(':alias', $data[$i]['alias']);
            $sth -> bindParam(':content', $data[$i]['content']);
            $sth -> bindParam(':date_create', $data[$i]['date_create']);
            $sth -> execute();
        }
        
    } // dbMultiInsert()


    public function testConnection()
    {
        $table = new Table();
        $table -> setConnection(new PDO('sqlite:./data/database/test_database.sqlite'));
        $this -> assertInstanceOf('\PDO', $table -> getConnection());
    } // testConnection()


    public function testTableName()
    {
        $table = new Table();
        $this -> assertEquals('table', $table -> getTableName());

        $table -> setTableName('pages');
        $this -> assertEquals('pages', $table -> getTableName());
    } // testTableName()


    public function testFetch()
    {
        $data = $this -> generateData();
        $this -> dbMultiInsert($data);

        $findings = array();
        foreach($this -> table -> fetch('select * from pages') as $item) {
            array_shift($item);
            $findings[] = $item;
        }
        $this -> assertEquals($data, $findings);
    } // testFetch()


    public function testFetchAll()
    {
        $data = $this -> generateData();
        $this -> dbMultiInsert($data);

        $findings = $this -> table -> fetchAll('select * from pages');

        for($i = 0; $i < count($findings); $i++) {
            array_shift($findings[$i]);
        }
        $this -> assertEquals($data, $findings);
    } // testFetchAll()


    public function testFetchOne()
    {
        $data = $this -> generateData();
        $this -> dbMultiInsert($data);

        $findings = $this -> table -> fetchOne('select * from pages');
        array_shift($findings);
        $this -> assertEquals($data[0], $findings);
    } // testFetchOne()
    

    public function testGetByPk()
    {
        $data = $this -> generateData();
        $this -> dbMultiInsert($data);

        $result = $this -> table -> getByPk(2);
        $this -> assertEquals(2, $result['id']);
        $this -> assertCount(5, $result);
    } // testGetByPk()


    public function testGetByPkForMultiPrimaryKeyAsArray()
    {
        $this -> table
            -> setTableName('pages_translations')
            -> setPrimaryKey('page_id', 'lang_id');

        $this -> table -> getConnection() -> exec(
            'insert into pages_translations values (1, 2, "test_content"), (2, 1, "test_content"), (3, 1, "test_content")'
        );

        $result = $this -> table -> getByPk(array(2, 1));
        $this -> assertEquals(array('page_id' => 2, 'lang_id' => 1, 'content' => 'test_content'), $result);
    } // testGetByPkForMultiPrimaryKeyAsArray()


    public function testGetByPkForMultyPrimaryKeyAsAruments()
    {
        $this -> table
            -> setTableName('pages_translations')
            -> setPrimaryKey('page_id', 'lang_id');

        $this -> table -> getConnection() -> exec(
            'insert into pages_translations values (1, 2, "test_content"), (2, 1, "test_content"), (3, 1, "test_content")'
        );

        $result = $this -> table -> getByPk(2, 1);
        $this -> assertEquals(array('page_id' => 2, 'lang_id' => 1, 'content' => 'test_content'), $result);
    } // testGetByPkForMultyPrimaryKeyAsAruments()


    public function testGetByIdForSinglePrimaryKeyAndMultyArguments()
    {
        $this -> setExpectedException('Core\Db\Sql\Exception\InvalidArgumentException', 'Too much values for where statement');
        $this -> table -> getByPk(array(2, 1));
    } // testGetByIdForSinglePrimaryKeyAndMultyArguments()


    public function testInsert()
    {
        $data = $this -> generateData(1);

        $this -> table -> insert($data);

        $qb = new QueryBuilder();
        $qb -> select() -> where() -> equal('id', $this -> table -> getConnection() -> lastInsertId());

        $findings = $this -> table -> fetchOne($qb);
        array_shift($findings);

        $this -> assertEquals($data, $findings);
    } // testInsert()


    public function testUpdate()
    {
        $data       = $this -> generateData(5);
        $updateData = array(
            'title'   => 'test_title',
            'alias'   => 'test_alias',
            'content' => 'test_content',
            'date_create' => '99999999'
        );

        $this -> dbMultiInsert($data);
        $this -> table -> update($updateData, 2);
        $resul = $this -> table -> getByPk(2);

        $updateData['id'] = 2;

        $this -> assertEquals($updateData, $resul);
    } // update()


    public function testDelete()
    {
        $data = $this -> generateData(5);
        $this -> dbMultiInsert($data);
        
        $this -> table -> delete(2);
        
        $rowsNum = current($this -> table -> fetchOne('select count(*) from pages'));
        $this -> assertEquals(4, $rowsNum);
    } // testDelete()


    public function testWhere()
    {
        $data = array(
            'title'   => 'test_title',
            'alias'   => 'test_alias',
            'content' => 'test content test',
            'date_create' => '99999999'
        );

        $this -> table -> insert($data);
        $sql = (new QueryBuilder) -> where() -> like('content', '%content%');
        $result = $this -> table -> fetchOne($sql);

        $data['id'] = 1;

        $this -> assertEquals($data, $result);
    } // testWhere()


    public function testFindBuilder()
    {
        $data = array(
            'title'   => 'test_title',
            'alias'   => 'test_alias',
            'content' => 'test content test',
            'date_create' => '99999999'
        );

        $this -> table -> insert($data);
        $result = $this -> table -> findOneByAlias('test_alias');
        
        $data['id'] = 1;

        $this -> assertEquals($data, $result);
    } // testFindBuilder()
} 