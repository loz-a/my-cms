<?php
namespace Core\Stdlib\Entity;

require_once './tests/_fixture/ClassMethodsEntity.php';

class ClassMethodsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \ClassMethodsEntity
     */
    protected $entity;

    public function setUp()
    {
        $this -> entity = new \ClassMethodsEntity();
    } // setUp()


    public function testFakeSetMethods()
    {
        $value = 'title';
        $this -> entity -> setTitle($value);
        $this -> assertEquals($value, $this -> entity -> getTitle());
    } // testSetMethods()


    public function testSetMethods()
    {
        $value = 'text';
        $this -> entity -> setText($value);
        $this -> assertEquals($value, $this -> entity -> getText());
    } // testSetMethods()


    public function testThrowInvalidPropertyExceptionInSetMethod()
    {
        $this -> setExpectedException('Core\Stdlib\Entity\Exception\InvalidPropertyException');
        $this -> entity -> setName('name');
    } // testThrowInvalidPropertyException()


    public function testThrowInvalidPropertyExceptionInGetMethod()
    {
        $this -> setExpectedException('Core\Stdlib\Entity\Exception\InvalidPropertyException');
        $this -> entity -> getName();
    } // testThrowInvalidPropertyException()


    public function testThrowBadMethodCallException()
    {
        $this -> setExpectedException('Core\Stdlib\Entity\Exception\BadMethodCallException');
        $this -> entity -> greating();
    } // testThrowBadMethodCallException()
}