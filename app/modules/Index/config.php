<?php
return array(

    'routes' => array(
        'home' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/',
                'defaults' => array(
                    'controller' => 'index.controller.index',
                    'action'     => 'index'
                )
            )
        )
    ),

    'view' => array(
        'template_path_stack' => array(
            __DIR__ . '/templates'
        ),
        'template_map' => array(
            'index/index/index'       => __DIR__ . '/templates/index/index/index.phtml',
            'index/layout/layout'     => __DIR__ . '/templates/index/layout/layout.phtml',
            'index/layout/navigation' => __DIR__ . '/templates/index/layout/navigation.phtml',
            'index/layout/footer'     => __DIR__ . '/templates/index/layout/footer.phtml'
        )
    ),

    'service_manager' => array(
        'invokables' => array(
            'index.controller.index' => 'Index\Controller\Index'
        )
    ),

);