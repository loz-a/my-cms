<?php

use Core\App;

if (!function_exists('user_identity')) {

    /**
     * @return \Auth\Service\UserIdentity
     */
    function user_identity() {
        return App::getInstance() -> getServiceManager() -> get('auth.service.user_identity');
    }

}
