<?php
return array(

    'routes' => array(
        'login' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/login',
                'defaults' => array(
                    'controller' => 'auth.controller.login',
                    'action'     => 'index'
                )
            )
        ),

        'logout' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/logout',
                'defaults' => array(
                    'controller' => 'auth.controller.login',
                    'action'     => 'logout'
                )
            )
        )
    ),

    'view' => array(
        'template_path_stack' => array(
            __DIR__ . '/templates'
        ),
        'template_map' => array(
            'auth/login/index'        => __DIR__ . '/templates/auth/login/index.phtml',
            'index/layout/navigation' => __DIR__ . '/templates/index/layout/navigation.phtml',
        )
    ),

    'service_manager' => array(
        'invokables' => array(
            'auth.controller.login'              => 'Auth\Controller\Login',
            'mapper.users'                       => 'Auth\Mapper\Users',
            'mapper.unsuccessful_login_attempts' => 'Auth\Mapper\UnsuccessfulLoginAttempts',

            'Auth.Annotations.LoginRequired' => 'Auth\Annotations\LoginRequired'
        ),

        'factories' => array(
            'auth'                       => 'Auth\Core\Service\Factory\Auth',
            'auth.adapter'               => 'Auth\Core\Adapter\Factory\Adapter',
            'auth.listener'              => 'Auth\Core\Adapter\Factory\Listener',
            'auth.service.user_identity' => 'Auth\Service\Factory\UserIdentity'
        ),

        'aliases' => array(
            'annotations.login_required' => 'Auth.Annotations.LoginRequired'
        )
    ),

);