<?php
namespace Auth\Core\Adapter\Factory;

use Auth\Core\Adapter\Listener as AuthListener;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Listener implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator -> get('config');

        $options = array();
        $options['unsuccessful_login_count']          = $config -> get('unsuccessful_login_count', 3);
        $options['unsuccessful_login_pause_duration'] = $config -> get('unsuccessful_login_pause_duration', 30);
        $options['password_cost']                     = $config -> get('password_cost', 14);

        $usersMapper          = $serviceLocator -> get('mapper.users');
        $unsLogAttemptsMapper = $serviceLocator -> get('mapper.unsuccessful_login_attempts');

        return new AuthListener($options, $usersMapper, $unsLogAttemptsMapper);
    } // create()
} 