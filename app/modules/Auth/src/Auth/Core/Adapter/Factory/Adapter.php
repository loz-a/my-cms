<?php
namespace Auth\Core\Adapter\Factory;

use Auth\Core\Adapter\Adapter as AuthAdapter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class Adapter implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $adapter = new AuthAdapter();
        $adapter -> getEventManager() -> attachAggregate($serviceLocator -> get('auth.listener'));
        return $adapter;
    } // create()
} 