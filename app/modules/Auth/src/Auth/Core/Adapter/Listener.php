<?php
namespace Auth\Core\Adapter;

use Auth\Mapper\Users as UsersMapper;
use Auth\Mapper\UnsuccessfulLoginAttempts as UnsuccessfulLoginAttemptsMapper;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Crypt\Password\Bcrypt;
use Auth\Core\Result as AuthResult;

class Listener implements ListenerAggregateInterface
{
    /**
     * @var array
     */
    protected $listeners;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var UsersMapper
     */
    protected $usersMapper;

    /**
     * @var UnsuccessfulLoginAttemptsMapper
     */
    protected $unsuccessfulLoginAttemptsMapper;


    public function __construct(
        array $options,
        UsersMapper $usersMapper,
        UnsuccessfulLoginAttemptsMapper $unsLogAttemptsMapper
    ) {
        $this -> options = $options;
        $this -> usersMapper = $usersMapper;
        $this -> unsuccessfulLoginAttemptsMapper = $unsLogAttemptsMapper;
    } // __construct()


    /**
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $this -> listeners[] = $events -> attach('authenticate', array($this, 'authenticate'));
        $this -> listeners[] = $events -> attach('authenticate.post', array($this, 'authenticateAfter'));
    } // attach()


    /**
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ( $this -> listeners as $key => $listener ) {
            $events -> detach($listener);
        }
    } // detach()


    public function authenticate(Event $e)
    {
        $identity = $e -> getTarget() -> getIdentity();

        /** @var \Auth\Entity\User $user */
        $user = $this -> usersMapper -> findUserByLogin($identity);

        if (!$user) {
            $e -> setCode(AuthResult::FAILURE_IDENTITY_NOT_FOUND)
               -> setMessage('A record with the supplied identity could not be found.');
            return false;
        }

        $e -> setIdentity($user -> getId());
        $credential = $e -> getTarget() -> getCredential();

        // Brute force protection
        $userLogData  = $this -> unsuccessfulLoginAttemptsMapper -> findById($user -> getId());
        $options      = $this -> options;

        if ($userLogData) {

            if (intval($userLogData['count']) > $options['unsuccessful_login_count']
                and time() < ($userLogData['attempt_time'] + $options['unsuccessful_login_pause_duration'])
            ) {
                $e -> setCode(AuthResult::FAILURE_TIMEOUT);
                $e -> setMessage(sprintf('Authorization error. Please, expect the next %s seconds to login.', $options['unsuccessful_login_pause_duration']));
                return false;
            }
        }

        $bcrypt = new Bcrypt();
        $bcrypt -> setCost($options['password_cost']);
        $result = $bcrypt -> verify($credential, $user -> getPassword());

        if ( !$result ) {
            $e -> setCode(AuthResult::FAILURE_CREDENTIAL_INVALID)
               -> setMessage('Supplied credential is invalid.');
        }
        else {
            $e -> setCode(AuthResult::SUCCESS)
               -> setMessage('Authentication successful.');
        }

        return $result;
    } // authenticate()


    public function authenticateAfter(Event $e)
    {
        switch($e -> getCode()) {
            case AuthResult::SUCCESS:
                $this -> resetUnsuccessfulLogData($e);
                break;
            case AuthResult::FAILURE_CREDENTIAL_INVALID:
                $this -> updateUnsuccessfulLogData($e);
                break;
            case AuthResult::FAILURE_TIMEOUT:
                $this -> updateUnsuccessfulLogData($e);
                break;
        }
    } // authenticateAfter()


    protected function updateUnsuccessfulLogData(Event $e)
    {
        $userId = $e -> getIdentity();
        $userLogData = $this -> unsuccessfulLoginAttemptsMapper -> findById($userId);

        if (!$userLogData) {
            $this -> unsuccessfulLoginAttemptsMapper -> insert(array(
                'id'    => $userId,
                'count' => 1,
                'attempt_time' => time()
            ));
        }
        else {
            $data = array();
            $data['count'] = ++$userLogData['count'];
            $data['attempt_time'] = time();
            $this -> unsuccessfulLoginAttemptsMapper -> update($data, $userId);
        }
    } // updateUnsuccessfulLogData()


    public function resetUnsuccessfulLogData(Event $e)
    {
        $this -> unsuccessfulLoginAttemptsMapper -> update(array('count' => 0), $e -> getIdentity());
    } // resetUnsuccessfulLogData()

} // Listener
