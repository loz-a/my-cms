<?php
namespace Auth\Core\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\AdapterInterface;
use Auth\Core\Exception\LogicException;
use Auth\Core\Adapter\Adapter as AuthAdapter;

class Auth
{
    /**
     * @var AuthenticationService
     */
    protected $auth;


    /**
     * @param AuthAdapter $adapter
     */
    public function __construct(AuthAdapter $adapter)
    {
        $this -> auth = new AuthenticationService();
        $this -> auth -> setAdapter($adapter);
        return $this;
    } // __construct()


    /**
     * @return AuthenticationService
     * @throws \Auth\Core\Exception\LogicException
     */
    protected function auth()
    {
        if (!$this -> auth) {
            throw new LogicException('Service was not initialized');
        }
        return $this -> auth;
    } // getBaseAuth()


    /**
     * @return bool
     */
    public function hasIdentity()
    {
        return $this -> auth() -> hasIdentity();
    } // hasIdentity()


    /**
     * @return mixed|null
     */
    public function getIdentity()
    {
        return $this -> auth() -> getIdentity();
    } // getIdentity()


    /**
     * @param AdapterInterface $adapter
     * @return \Zend\Authentication\Result
     */
    public function authenticate(AdapterInterface $adapter = null)
    {
        return $this -> auth() -> authenticate($adapter);
    } // authenticate()


    /**
     * @param $login
     * @param $password
     * @return \Zend\Authentication\Result
     */
    public function login($login, $password)
    {
        if ($this -> auth() -> hasIdentity()) {
            $this -> auth() -> clearIdentity();
        }

        $this
            -> auth()
            -> getAdapter()
            -> setIdentity($login)
            -> setCredential($password);

        return $this -> auth() -> authenticate();
    } // login()


    public function logout()
    {
        $this -> auth() -> clearIdentity();
    } // logout()

} // Auth
