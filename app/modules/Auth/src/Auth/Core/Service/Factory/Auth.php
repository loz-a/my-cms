<?php
namespace Auth\Core\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Auth\Core\Service\Auth as AuthService;

class Auth implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new AuthService($serviceLocator -> get('auth.adapter'));
    } // createService()
} 