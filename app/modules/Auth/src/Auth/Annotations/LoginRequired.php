<?php
namespace Auth\Annotations;

use Core\App;
use Core\App\Controller\Action\RedirectTrait;

/**
 *  Annotation example:
 *  @Auth.Annotations.LoginRequired
 */

class LoginRequired
{
    use RedirectTrait;

    public function before()
    {
        $sm  = $this -> getApplication() -> getServiceManager();

        if (!$sm -> get('auth') -> hasIdentity()) {
            $sm -> get('flashMessenger') -> addErrorMessage('Access Denied');
            $this -> redirect($this -> getApplication() -> getRoutesManager() -> urlFor('home'));
        }
    } // before()
    /**
     * @return \Core\App
     */
    public function getApplication()
    {
        return App::getInstance();
    }
}
