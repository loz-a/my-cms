<?php
namespace Auth\Service\Factory;

use Auth\Service\UserIdentity as UserIdentityService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserIdentity implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $userIdentity = new UserIdentityService();
        $userIdentity
            -> setAuthService($serviceLocator -> get('auth'))
            -> setMapper($serviceLocator -> get('mapper.users'));

        return $userIdentity;
    } // createService()
} 