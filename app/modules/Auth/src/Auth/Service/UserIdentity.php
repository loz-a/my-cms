<?php
namespace Auth\Service;

use Auth\Mapper\Users as UsersMapper;
use Auth\Core\Service\Auth as AuthService;

class UserIdentity
{
    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * @var UsersMapper
     */
    protected $userMapper;

    /**
     * @var \stdClass
     */
    protected $user;

    /**
     * @return \stdClass
     */
    public function getIdentity()
    {
        $user = $this -> user;
        $auth = $this -> getAuthService();

        if (!$auth -> hasIdentity()) {
            $this -> user = null;
            return null;
        }

        $identity = $auth -> getIdentity();
        if (!$user or $user -> id != $identity) {
            $user = $this -> getMapper() -> findUserById($identity);
            if (!$user) {
                return null;
            }
            $user -> password = null;
            $this -> user = (object) $user;
        }
        return $this -> user;
    } // getIdentity()


    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this -> getIdentity() -> {$name};
    } // __get()


    /**
     * @return bool
     */
    public function hasIdentity()
    {
        return $this -> getAuthService() -> hasIdentity();
    } // hasIdentity()


    /**
     * @param AuthService $authService
     * @return $this
     */
    public function setAuthService(AuthService $authService)
    {
        $this -> authService = $authService;
        return $this;
    } // setAuthService()

    /**
     * @return AuthService
     * @throws Exception\InvalidAuthServiceException
     */
    public function getAuthService()
    {
        if (!$this -> authService) {
            throw new Exception\InvalidAuthServiceException('Auth service is undefined');
        }
        return $this -> authService;
    } // getAuthService()


    /**
     * @param UsersMapper $mapper
     * @return $this
     */
    public function setMapper(UsersMapper $mapper)
    {
        $this -> userMapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return UsersMapper
     * @throws Exception\InvalidMapperException
     */
    protected function getMapper()
    {
        if (!$this -> userMapper) {
            throw new Exception\InvalidMapperException('User mapper is undefined');
        }
        return $this -> userMapper;
    } // getMapper()

} // UserIdentity
