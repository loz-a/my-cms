<?php
namespace Pages;

use Core\App as Application;
use Zend\Loader\AutoloaderFactory;

class Init
{
    public function __invoke(Application $app)
    {
        AutoloaderFactory::factory(array(
            //        'Zend\Loader\ClassMapAutoloader' => array(
            //            __DIR__ . '/autoload_classmap.php',
            //        ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        ));

    } // __invoke()
} 