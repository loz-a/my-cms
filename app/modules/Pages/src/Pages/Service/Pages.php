<?php
namespace Pages\Service;

use Core\View\Model\JsonErrorModel;
use Parsedown;
use Pages\Mapper\Pages as PagesMapper;
use Pages\Filter\Translit;
use Zend\Http\PhpEnvironment\Request;

class Pages
{
    /**
     * @var  PagesMapper
     */
    protected $mapper;


    /**
     * @param array $data
     * @return array|null
     */
    public function create(array $data)
    {
        $data['slug']              = (new Translit()) -> filter($data['title']);
        $data['parsed_content']    = (new Parsedown()) -> text($data['preparsed_content']);
        $data['created']           = time();
        $data['updated']           = $data['created'];
        $data['author']            = 1;

        $mapper     = $this     -> getMapper();
        $hydrator   = $mapper   -> getHydrator();
        $pageEntity = $hydrator -> hydratePage($data);

        $insertedId = $mapper -> insert($pageEntity);

        if (!$insertedId) {
            return new JsonErrorModel();
        }

        return $hydrator -> extract($mapper -> getByPk($insertedId));
    } // create()


    public function update(array $data, $id)
    {
        $data['parsed_content']    = (new Parsedown()) -> text($data['preparsed_content']);
        $data['updated']           = time();

        $mapper     = $this   -> getMapper();
        $hydrator   = $mapper -> getHydrator();
        $pageEntity = $mapper -> getByPk($id);

        if (null === $pageEntity) {
            return new JsonErrorModel();
        }

        $pageEntity = $hydrator -> hydrate($data, $pageEntity);
        $mapper -> update($pageEntity);
        return $hydrator -> extract($mapper -> getByPk($id));
    } // update()


    public function remove($id)
    {
        $mapper     = $this   -> getMapper();
        $pageEntity = $mapper -> getByPk($id);

        if (!$pageEntity) {
            return new JsonErrorModel();
        }
        $result = $mapper -> remove($id);

        return $mapper -> getHydrator() -> extract($pageEntity);
    } // remove()


    public function getPage($id)
    {
        return $this -> getMapper() -> getByPk($id);
    } // getPage()


    public function getPageBySlug($slug)
    {
        return $this -> getMapper() -> getBySlug($slug);
    } // getPageBySlug()


    public function getPages($limit, $offset, $predicates)
    {
        return $this -> getMapper() -> fetchAll($limit, $offset, $predicates);
    } // getPages()


    public function getPagesTotal()
    {
        return $this -> getMapper() -> getPagesTotal();
    } // getPagesTotal()


    /**
     * @param PagesMapper $mapper
     * @return $this
     */
    public function setMapper(PagesMapper $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return PagesMapper
     * @throws Exception\UndefinedPagesMapperException
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\UndefinedPagesMapperException('Pages mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()
}