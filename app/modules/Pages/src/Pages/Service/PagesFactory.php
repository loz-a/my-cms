<?php
namespace Pages\Service;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PagesFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $pagesService = new Pages();
        $pagesService -> setMapper($serviceLocator -> get('pages.mapper'));
        return $pagesService;
    }

} 