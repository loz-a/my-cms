<?php
namespace Pages\Validator;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Page extends InputFilter
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl      = $this -> getServiceLocator();
        $options = $sl   -> get('pages.options');
        $id      = (int) $sl -> get('App') -> getParams() -> get('id');

        if ( $id ) {
            $this
                -> add(array(
                    'name' => 'id',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int')
                    )
                ));
        }

        $this
            -> add(array(
                'name'     => 'alias',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getAliasMaxLength()
                        )
                    ),
                    array(
                        'name' => 'Pages\Validator\NoRecordExists',
                        'options' => array(
                            'mapper'  => $sl -> get('pages.mapper'),
                            'key'     => 'alias',
                            'exclude' => $id ?: null
                        )
                    )
                )
            ))
            -> add(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getTitleMaxLength()
                        )
                    ),
//                    array(
//                        'name' => 'CMSPages\Validator\NoRecordExists',
//                        'options' => array(
//                            'mapper'  => $this -> serviceManager -> get('CMSPages\Mapper'),
//                            'key'     => 'head_title',
//                            'exclude' => $id ? sprintf('page_id != %s', $id) : null
//                        )
//                    )
                )
            ))
            -> add(array(
                'name'     => 'description',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getDescriptionMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name'     => 'preparsed_content',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                )
            ))
            -> add(array(
                'name'     => 'status',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int')
                )
            ));


    } // init()
} 