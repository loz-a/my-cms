<?php
namespace Pages\Validator\Exception;

class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

} 