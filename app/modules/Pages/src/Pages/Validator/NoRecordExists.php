<?php
namespace Pages\Validator;


use Core\Stdlib\Validator\AbstractRecord;
use Core\Stdlib\Validator\Exception\InvalidKeyException;
use Pages\Mapper\PagesInterface;

class NoRecordExists extends AbstractRecord
{
    /**
     * @param $mapper
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function setMapper($mapper)
    {
        if (!$mapper instanceof PagesInterface) {
            throw new Exception\InvalidArgumentException('Invalid mapper. Expected instance of Pages\Mapper\PagesInterface');
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @param mixed $value
     * @return bool|void
     */
    public function isValid($value)
    {
        $this -> setValue($value);

        $result = $this -> query($value);
        if ($result) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    } // isValid()


    protected function query($value)
    {
        $key    = $this -> getKey();
        $result = false;

        switch ($key) {
            case 'alias':
                $result = $this -> getMapper() -> lookupByAlias($value, $this -> getExclude());
                break;
            case 'slug':
                $result = $this -> getMapper() -> lookupBySlug($value, $this -> getExclude());
                break;
            default:
                throw new InvalidKeyException(sprintf('Invalid key - %s'), $key);
        }
        return $result;
    } // query()

} 