<?php
namespace Pages\Mapper;

use Pages\Entity\PageInterface;

interface PagesInterface
{
    public function fetchAll($limit, $offset, array $predicates = null);

    public function insert(PageInterface $page);

    public function update(PageInterface $page);

    public function remove($id);

    public function lookupByAlias($alias, $excludeId = null);

    public function lookupBySlug($slug, $excludeId = null);
} 