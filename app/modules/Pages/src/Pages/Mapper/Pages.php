<?php
namespace Pages\Mapper;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Table\DbTableAwareInterface;
use Core\Db\Table\DbTableAwareTrait;
use Core\Stdlib\Hydrator\HydratorAwareTrait;
use Pages\Entity\Page;
use Pages\Entity\PageInterface;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;

class Pages implements PagesInterface, DbTableAwareInterface, HydratorAwareInterface
{
    use DbTableAwareTrait, HydratorAwareTrait;

    /**
     * @param $limit
     * @param $offset
     * @param array $predicates
     * @return array
     */
    public function fetchAll($limit, $offset, array $predicates = null)
    {
        $dbTable = $this -> getDbTable();

        $qb = $this -> createSelectQueryBuilder($dbTable);
        $qb -> limit($limit, $offset);

        if ($predicates) {
            foreach ($predicates as $identifier => $value) {
                $qb -> where($identifier, $value);
            }
        }

        return $dbTable -> fetchAll($qb);
    } // fetchAll()


    /**
     * @param $id
     * @return null|PageInterface
     */
    public function getByPk($id)
    {
        $dbTable = $this -> getDbTable();

        $qb = $this -> createSelectQueryBuilder($dbTable);
        $qb -> where('p.id', $id);
        $qb -> limit(1);

        $result = $dbTable -> fetchOne($qb);

        if (!$result) {
            return null;
        }
        return $this -> getHydrator() -> hydratePage($result);
    } // getByPk()


    public function getBySlug($slug)
    {
        $dbTable = $this -> getDbTable();

        $qb = $this -> createSelectQueryBuilder($dbTable);
        $qb -> where('p.slug', $slug);
        $qb -> limit(1);

        $result = $dbTable -> fetchOne($qb);

        if (!$result) {
            return null;
        }
        return $this -> getHydrator() -> hydratePage($result);
    } // getBySlug()


    public function getPagesTotal()
    {
        $dbTable = $this -> getDbTable();
        $qb = new QueryBuilder();
        $qb -> select() -> count();
        return $dbTable -> fetchColumn($qb);
    } // getPagesTotal()


    /**
     * @param PageInterface $page
     * @return null|string
     * @throws Exception\FailedInsertException
     */
    public function insert(PageInterface $page)
    {
        $table      = $this  -> getDbTable();
        $connection = $table -> getConnection();
        $pageId     = null;
        $localeId   = 1;

        $connection -> beginTransaction();
        try {

            $table -> insert(
                array(
                    'alias'     => $page -> getAlias(),
                    'slug'      => $page -> getSlug(),
                    'status'    => $page -> getStatus(),
                    'created'   => $page -> getCreated(),
                    'updated'   => $page -> getUpdated(),
                    'author_id' => $page -> getAuthor()
                )
            );

            $pageId = $connection -> lastInsertId('my_cms');

            $table -> insert(
                array(
                    'page_id'     => $pageId,
                    'locale_id'   => $localeId,
                    'title'       => $page -> getTitle(),
                    'description' => $page -> getDescription(),
                    'content'     => $page -> getContent() -> getParsedText()
                ),
                $table -> getRelatedTableName('pages_translations')
            );

            $table -> insert(
                array(
                    'id'      => $connection -> lastInsertId(),
                    'content' => $page -> getContent() -> getRawText()
                ),
                $table -> getRelatedTableName('pages_preparsed_content')
            );

        }
        catch (\Exception $e) {
            $connection -> rollBack();
            throw new Exception\FailedInsertException('Page can\'t updated');
        }
        $connection -> commit();

        return $pageId;
    }


    public function update(PageInterface $page)
    {
        $table        = $this  -> getDbTable();
        $connection   = $table -> getConnection();
        $pageId       = $page -> getId();
        $translatinId = $page -> getTranslationId();
        $localeId     = 1;

        $connection -> beginTransaction();
        try {

            $table -> update(
                array(
                    'alias'     => $page -> getAlias(),
                    'status'    => $page -> getStatus(),
                    'updated'   => $page -> getUpdated()
                ),
                $pageId
            );

            if (!$translatinId) {

                $table -> insert(
                    array(
                        'title'       => $page -> getTitle(),
                        'description' => $page -> getDescription(),
                        'content'     => $page -> getContent() -> getParsedText()
                    ),
                    $table -> getRelatedTableName('pages_translations')
                );

                $table -> insert(
                    array(
                        'id'      => $connection -> lastInsertId(),
                        'content' => $page -> getContent() -> getRawText()
                    ),
                    $table -> getRelatedTableName('pages_preparsed_content')
                );
            }
            else {
                $table -> update(
                    array(
                        'title'       => $page -> getTitle(),
                        'description' => $page -> getDescription(),
                        'content'     => $page -> getContent() -> getParsedText()
                    ),
                    $translatinId, $table -> getRelatedTableName('pages_translations')
                );

                $table -> update(
                    array(
                        'content' => $page -> getContent() -> getRawText()
                    ),
                    $translatinId, $table -> getRelatedTableName('pages_preparsed_content')
                );
            }

        }
        catch (\Exception $e) {
            $connection -> rollBack();
            throw new Exception\FailedUpdateException('Page can\'t updated');
        }
        $connection -> commit();

        return $pageId;
    }


    /**
     * @param $id
     * @return int
     */
    public function remove($id)
    {
        return $this -> getDbTable() -> delete($id);
    }
    // fetchAll()


    public function lookupByAlias($alias, $excludeId = null)
    {
        $dbTable = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($dbTable -> getTableName())
            -> where('alias', $alias)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $dbTable -> fetchOne($qb);
    } // lookupByAlias()


    public function lookupBySlug($slug, $excludeId = null)
    {
        $dbTable = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($dbTable -> getTableName())
            -> where('slug', $slug)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $dbTable -> fetchOne($qb);
    } // lookupBySlug()


    /**
     * @return mixed
     */
    protected function createSelectQueryBuilder($dbTable)
    {
        $qb = new QueryBuilder();
        $qb -> select(
                'p.id', 'p.alias', 'p.slug', 'p.status', 'p.created', 'p.updated',
                'p.author_id', 't.id as translation_id', 't.title', 't.description', 't.content as parsed_content',
                'pc.content as preparsed_content'
            )
            -> from($dbTable -> getTableName(), 'p')
            -> join()
                -> left($dbTable -> getRelatedTableName('pages_translations'), 't')
                -> on('p.id = t.page_id')
                -> andEqual('t.locale_id', 1)
            -> join()
                -> left($dbTable -> getRelatedTableName('pages_preparsed_content'), 'pc')
                -> on('t.id = pc.id');

        return $qb;
    } // createSelectQueryBuilder()
}