<?php
namespace Pages\Controller;

use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\App\Controller\AbstractController;
use Core\App\Exception\PageNotFoundException;

class Index extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.template pages/index/index
     * @annotations.layout   admin/layout/layout
     * @annotations.login_required
     */
    public function indexAction()
    {
        return array();
    } // indexAction()


    /**
     * @annotations.template pages/index/view
     * @annotations.layout   index/layout/layout
     * @annotations.route_match str slug
     */
    public function viewAction($slug)
    {
        if (!$slug) {
            throw new PageNotFoundException('Slug is undefined');
        }

        $page = $this -> get('pages.service') -> getPageBySlug($slug);
        if (!$page) {
            throw new PageNotFoundException(sprintf('Page with slug "%s" is undefined', $slug));
        }

        $created = gmdate("D, d M Y H:i:s", strtotime($page -> getCreated()));
        $updated = gmdate("D, d M Y H:i:s", strtotime($page -> getUpdated()));

        $this
            -> get('request')
            -> getHeaders()
            -> addHeaders(array(
                sprintf('Date: %s GMT', $created),
                sprintf('Last-Modified: %s GMT', $updated)
            ));

        return array('page' => $page);
    } // viewAction()
} 