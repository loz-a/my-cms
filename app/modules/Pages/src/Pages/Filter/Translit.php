<?php
namespace Pages\Filter;

use Core\Stdlib\Filter\Translit as BaseTranslit;

class Translit extends BaseTranslit
{
    public function __construct()
    {
        $this -> setSpaceReplacer('-');
    } // __construct()


    public function filter($value)
    {
        $value = parent::filter($value);
        return strtolower(preg_replace('/[^a-zA-Z0-9_\-]/', '', $value));
    } // filter()
} 