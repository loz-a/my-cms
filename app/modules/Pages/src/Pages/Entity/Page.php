<?php
namespace Pages\Entity;


class Page implements PageInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $translationId;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var ContentInterface
     */
    protected $content;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var int
     */
    protected $created;

    /**
     * @var int
     */
    protected $updated;

    /**
     * @var string
     */
    protected $author;

    /**
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this -> alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this -> alias;
    }


    /**
     * @param string $author
     * @return $this
     */
    public function setAuthor($author)
    {
        $this -> author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this -> author;
    }


    /**
     * @param ContentInterface $content
     * @return $this
     */
    public function setContent(ContentInterface $content)
    {
        $this -> content = $content;
        return $this;
    }

    /**
     * @return ContentInterface
     */
    public function getContent()
    {
        return $this -> content;
    }


    /**
     * @param int $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this -> created = $created;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this -> created;
    }


    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this -> description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this -> description;
    }


    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this -> id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this -> id;
    }


    /**
     * @param int $id
     * @return $this
     */
    public function setTranslationId($id)
    {
        $this -> translationId = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTranslationId()
    {
        return $this -> translationId;
    }


    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this -> slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this -> slug;
    }


    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this -> status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this -> status;
    }


    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this -> title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this -> title;
    }


    /**
     * @param int $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this -> updated = $updated;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this -> updated;
    }

} 