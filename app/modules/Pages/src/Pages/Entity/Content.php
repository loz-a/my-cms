<?php
namespace Pages\Entity;

class Content implements ContentInterface
{
    /**
     * @var string
     */
    protected $rawText;

    /**
     * @var string
     */
    protected $parsedText;

    /**
     * @return string
     */
    public function getRawText()
    {
        return $this -> rawText;
    } // getRawText()


    /**
     * @param $rawText
     * @return $this
     */
    public function setRawText($rawText)
    {
        $this -> rawText = $rawText;
        return $this;
    } // setRawText()


    /**
     * @return string
     */
    public function getParsedText()
    {
        return $this -> parsedText;
    } // getParsedText()


    /**
     * @param $parsedText
     * @return $this
     */
    public function setParsedText($parsedText)
    {
        $this -> parsedText = $parsedText;
        return $this;
    } // setParsedText()
} 