<?php
namespace Pages\Entity;


interface ContentInterface
{
    public function getRawText();

    public function setRawText($rawText);

    public function getParsedText();

    public function setParsedText($parsedText);
} 