<?php
namespace Pages\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements PageInterface
{
    /**
     * @var int
     */
    protected $aliasMaxLength = 255;

    /**
     * @param $maxLength
     */
    public function setAliasMaxLength($maxLength)
    {
        $this -> aliasMaxLength = (int) $maxLength;
    }

    /**
     * @return int
     */
    public function getAliasMaxLength()
    {
        return $this -> aliasMaxLength;
    }


    /**
     * @var int
     */
    protected $titleMaxLength = 255;

    /**
     * @param $maxLength
     */
    public function setTitleMaxLength($maxLength)
    {
        $this -> titleMaxLength = (int) $maxLength;
    }

    /**
     * @return int
     */
    public function getTitleMaxLength()
    {
        return $this -> titleMaxLength;
    }


    /**
     * @var int
     */
    protected $descriptionMaxLength = 510;

    /**
     * @param $maxLength
     */
    public function setDescriptionMaxLength($maxLength)
    {
        $this -> descriptionMaxLength = (int) $maxLength;
    }

    /**
     * @return int
     */
    public function getDescriptionMaxLength()
    {
        return $this -> descriptionMaxLength;
    }

} 