<?php

namespace Pages\Options;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator -> get('config');
        $options = isset($config['pages_options']) ? $config['pages_options'] : array();
        return new ModuleOptions($options);
    }
}