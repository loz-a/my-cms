<?php
namespace Pages\Hydrator;

use Zend\Stdlib\Hydrator\ClassMethods;
use Pages\Entity\Content as ContentEntity;

class Content extends ClassMethods
{
    public function hydrate(array $data, $object)
    {
        $hydrateData = array(
            'raw_text'    => $data['preparsed_content'],
            'parsed_text' => $data['parsed_content']
        );

        $content = parent::hydrate($hydrateData, new ContentEntity());
        $object -> setContent($content);
        return $object;
    } // hydrate()
} 