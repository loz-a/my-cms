<?php
return array(

    'routes' => array(
        'pages' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/pages',
                'defaults' => array(
                    'controller' => 'pages.controller.index',
                    'action'     => 'index'
                )
            )
        ),
        'page' => array(
            'type'  => 'Regex',
            'options' => array(
                'regex' => '/(?<slug>[a-z0-9_\-]+)\.html',
                'defaults' => array(
                    'controller' => 'pages.controller.index',
                    'action'     => 'view'
                ),
                'spec' => '/%slug%.html'
            )
        ),
        'pages_api' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/api/pages/',
                'defaults' => array(
                    'controller' => 'pages.controller.pages'
                )
            ),
            'may_terminate' => true,
            'child_routes'  => array(
                'id' => array(
                    'type'    => 'Segment',
                    'options' => array(
                        'route' => ':id',
                        'constraints' => array(
                            'id' => '\d+'
                        )
                    ),
                    'may_terminate' => true
                )
            )
        )
    ),

    'db' => array(
        'tables_names' => array(
            'pages' => array(
                'pages'                   => 'pages',
                'pages_translations'      => 'pages_translations',
                'pages_preparsed_content' => 'pages_preparsed_content',
                'pages_counts'            => 'pages_counts'
            )
        )
    ),


    'service_manager' => array(
        'invokables' => array(
            'pages.controller.index' => 'Pages\Controller\Index',
            'pages.controller.pages' => 'Pages\Controller\Pages',
            'pages.mapper'           => 'Pages\Mapper\Pages',
            'pages.hydrator'         => 'Pages\Hydrator\Page',
            'pages.validator'        => 'Pages\Validator\Page'
        ),
        'factories' => array(
            'pages.service' => 'Pages\Service\PagesFactory',
            'pages.options' => 'Pages\Options\Factory'
        )
    ),


    'view' => array(
        'template_path_stack' => array(
            __DIR__ . '/templates'
        ),
        'template_map' => array(
            'pages/index/index' => __DIR__ . '/templates/pages/index/index.phtml',
            'pages/index/view' => __DIR__ . '/templates/pages/index/view.phtml'
        )
    ),


    'admin_navigation' => array(
        array(
            'title'      => 'Pages',
            'icon.class' => 'fa fa-files-o',
            'matched.route.name' => 'pages',
            'order' => 2
        )
    )

);