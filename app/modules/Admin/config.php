<?php
return array(

    'routes' => array(
        'admin' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'   => '/admin',
                'defaults' => array(
                    'controller' => 'admin.controller.index',
                    'action'     => 'index'
                )
            )
        ),
    ),

    'view' => array(
        'template_path_stack' => array(
            __DIR__ . '/templates'
        ),
        'template_map' => array(
            'admin/index/index'       => __DIR__ . '/templates/admin/index/index.phtml',
            'admin/index/layout'      => __DIR__ . '/templates/admin/layout/layout.phtml',
            'admin/widget/navigation' => __DIR__ . '/templates/admin/widget/navigation.phtml'
        ),
        'widgets_service_manager' => array(
            'invokables' => array(
                'admin.navigation'     => 'Admin\Widget\Navigation',
                'admin.flash_messages' => 'Admin\Widget\FlashMessages'
            )
        )
    ),

    'service_manager' => array(
        'invokables' => array(
            'admin.controller.index' => 'Admin\Controller\Index'
        )
    ),

    'admin_navigation' => array(
        array(
            'title'      => 'Administrator',
            'icon.class' => 'fa fa-home fa-fw',
            'matched.route.name' => 'admin',
            'order' => 1
        )
    )

);