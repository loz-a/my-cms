<?php
namespace Admin\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;

class Index extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.template admin/index/index
     * @annotations.layout   admin/layout/layout
     * @annotations.login_required
     */
    public function indexAction()
    {
        return array();
    } // index()
}