<?php
namespace Admin\Widget;

use Core\App as Application;
use Core\View\Widget\AbstractWidget;

class FlashMessages extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $types = array(
            'default' => 'warning',
            'success' => 'success',
            'error'   => 'danger',
            'info'    => 'info'
        );

        $htmlOutput = array();

        $flashMessenger = $app -> getServiceManager() -> get('flashMessenger');

        foreach (array_keys($types) as $type) {
            $messages = $flashMessenger -> getMessagesFromNamespace($type);

            $hasMethod = sprintf('hasCurrent%sMessages', $type === 'default' ? '' : ucfirst($type));
            $hasCurrentMessages = call_user_func(array($flashMessenger, $hasMethod));

            if ($hasCurrentMessages) {
                $messages += $flashMessenger -> getCurrentMessagesFromNamespace($type);
                $flashMessenger -> clearCurrentMessagesFromNamespace($type);
            }

            if ($messages) {
                $htmlOutput[] = "<div class='alert alert-{$types[$type]}'>";
                foreach ($messages as $message) {
                    $htmlOutput[] = sprintf(
                        '<strong>%s!</strong> %s<br>',
                        ucfirst($type),
                        htmlentities(ucfirst($message), ENT_HTML5));
                }
                $htmlOutput[] = '</div>';
            }
        }
        return $htmlOutput ? implode(PHP_EOL, $htmlOutput) : '';
    } // __invoke()
}
