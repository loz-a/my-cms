<?php
namespace Admin\Widget;

use Core\App as Application;
use Core\Stdlib\MinPriorityQueue;
use Core\View\Widget\AbstractWidget;

class Navigation extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $sm     = $app -> getServiceManager();
        $config = $sm -> get('config') -> admin_navigation;

        $queue = new MinPriorityQueue();
        $queue -> setExtractFlags(MinPriorityQueue::EXTR_DATA);

        $matchedRouteName = $sm -> get('route_match') -> getMatchedRouteName();
        $temp = null;
        
        foreach ($config as $item) {
            $temp = $item -> toArray();
            if ($item['matched.route.name'] === $matchedRouteName) {
                $temp['active'] = true;
            }
            $queue -> insert($temp, $temp['order']);
        }

        $model = array(
            'navigation' => $queue
        );

        return $sm -> get('view') -> getRenderer() -> render('admin/widget/navigation',$model);
    }

}