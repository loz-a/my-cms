<?php
return array(

    'modules' => array(
        'Index' => __DIR__ . '/modules/Index',
        'Admin' => __DIR__ . '/modules/Admin',
        'Auth'  => __DIR__ . '/modules/Auth',
        'Pages' => __DIR__ . '/modules/Pages',
    ),

    'db' => array(
        'dsn' => 'mysql:host=localhost;dbname=my_cms',
        'user' => 'root',
        'password' => 'AM23naRmysql'
    ),

    'auth' => array(
        'password_cost' => 14,
        'unsuccessful_login_count' => 3,
        'unsuccessful_login_pause_duration' => 30
    ),

    'service_manager' => array(
        'invokables' => array(
            'view.map_resolver'        => 'Core\View\Resolver\TemplateMapResolver',
            'view.path_stack_resolver' => 'Core\View\Resolver\TemplatePathStack',
            'view.helpers_manager'     => 'Core\View\Helper\Manager',
            'view_model.view'          => 'Core\View\Model\ViewModel',
            'view_model.json'          => 'Core\View\Model\JsonModel',

            'Core.Annotations.Action.RouteMatch' => 'Core\Annotations\Action\RouteMatch',
            'Core.Annotations.Action.Request'    => 'Core\Annotations\Action\Request',
            'Core.View.Annotations.Layout'       => 'Core\View\Annotations\Layout',
            'Core.View.Annotations.Template'     => 'Core\View\Annotations\Template',
            'Core.View.Annotations.JsonResponse' => 'Core\View\Annotations\JsonResponse',

            'flash_messenger' => 'Zend\Mvc\Controller\Plugin\FlashMessenger',
        ),

        'aliases' => array(
            'annotations.route_match'   => 'Core.Annotations.Action.RouteMatch',
            'annotations.request'       => 'Core.Annotations.Action.Request',
            'annotations.layout'        => 'Core.View.Annotations.Layout',
            'annotations.template'      => 'Core.View.Annotations.Template',
            'annotations.json_response' => 'Core.View.Annotations.JsonResponse'
        ),

        'factories' => array(
            'view.json_renderer' => 'Core\View\Renderer\Factory\JsonRenderer',
            'view.php_renderer'  => 'Core\View\Renderer\Factory\PhpRenderer',
            'view'               => 'Core\App\View\Factory',
            'db.connection'      => 'Core\App\Db\ConnectionFactory'
        ),

        'initializers' => array(
            'Core\Db\Table\Initializer',
            'Core\Stdlib\InputFilterInitializer'
        )
    )

);