<?php
namespace App;

use Core\App as Application;

class Init
{
    public function __invoke(Application $app)
    {
        $response = $app -> getServiceManager() -> get('response');

        $app -> getEventManager() -> attach('404', function() use ($response) {
            $content = file_get_contents(__DIR__ . '/modules/Index/templates/index/error/404.phtml');

            $response -> setStatusCode(404);
            $response -> setContent($content);
        });


        $app -> getEventManager() -> attach('500', function() use ($response) {
            $content = file_get_contents(__DIR__ . '/modules/Index/templates/index/error/500.phtml');

            $response -> setStatusCode(500);
            $response -> setContent($content);
        });

    } // __invoke()
} 