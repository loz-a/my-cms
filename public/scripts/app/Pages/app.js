require(
{
    aliases: [
        ['config', 'pages/config'],
        ['IndexController', 'pages/controller/Index'],
        ['PagesStore', 'pages/store/Pages'],
        ['PageModel', 'pages/store/model/Page'],
        ['PagesGridWidget', 'pages/widget/PagesGrid'],
        ['TBDropdownWidget', 'dtbootstrap/Dropdown'],
        ['DialogWidget', 'pages/widget/dialog/Dialog'],
        ['DialogTabsWidget', 'pages/widget/dialog/tabs/Tabs'],
        ['DialogFormWidget', 'pages/widget/dialog/form/Form']
    ]
},
[
    'config',
    'IndexController',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'core/BaseApp'
],
function(

    config,
    IndexController,
    lang,
    declare,
    BaseApp

) { "use strict";

    BaseApp
        .getInstance()
        .configure(config)
        .mixinControllers({
            Index: IndexController
        })
        .registerRoutes();
});