define([
    'DialogWidget',
    'PagesGridWidget',
    'TBDropdownWidget'
], function(DialogWidget, GridWidget, DropdownWidget) { "use strict";

    var app;
    var dialog;
    var grid;
    var pagesCollection;
    var status = 'All';
    var language = 'Eng';

    var IndexController = {

        init: function(application) {
            this.application(application);
            grid            = getGrid();
            pagesCollection = grid.collection;
            dialog          = getDialog();
            initLangList();
            initStatusList();
        },


        index: function() {
            dialog.hide();
            grid.show();
        }, // index()


        cancel: function() {
            this.application().route('/');
        }, // cancel()


        create: function() {
            grid.hide();
            dialog.formWidget.reset();
            dialog.show();
        }, // create()1


        submitForm: function() {
            var values = dialog.formWidget.get('values');
            var route  = this.application().route;

            var res = grid.collection.put(values);
            res.then(function(data) {
                route('/');
            }, function(errorData) {
                dialog.formWidget.set('errors', JSON.parse(errorData.response.data));
            });
        }, // submitForm()


        edit: function(evt, id) {

            grid.collection.get(id).then(function(data) {
                dialog.formWidget.set('values', data);
                dialog.show();
                grid.hide();
            });
        }, // edit()


        remove: function(evt , id) {
            var route = this.application().route;

            grid.collection.get(id).then(function(page) {
                if (confirm('Are you sure you want to delete page "' + page.alias + '"')) {
                    grid.collection.remove(id);
                }
//                route('/', true);  // inexplicable behavior
                location.hash = '/';
            });

        }, // remove()


        application: function(application) {
            if (arguments.length) {
                app = application;
                return this;
            }
            return app;
        } // application()
    };


    function getDialog() {
        var dialog = new DialogWidget();
        dialog.placeAt(document.getElementById('pages-app'));
        dialog.startup();


        dialog.hide();

        dialog.formWidget.onCancel = IndexController.cancel.bind(IndexController);
        dialog.formWidget.onSubmit = IndexController.submitForm.bind(IndexController);

        return dialog;
    } // getDialog()


    function getGrid() {
        var grid = new GridWidget({}, document.getElementById('pages-grid'));
        grid.startup();
        return grid;
    } // getGrid()


    function initStatusList() {
        var statusList = new DropdownWidget(document.getElementById('status-list'));
        statusList.watch('value', function(name, oldValue, value) {
            grid.set('collection', pagesCollection.filter({status: parseInt(value)}));
            status = value;
            IndexController.application().route('/');
        });
    } // initStatusList()


    function initLangList() {
        var langList = new DropdownWidget(document.getElementById('lang-list'));
        langList.watch('value', function(name, oldValue, value) {
            language = value;
        });
    } // initLangList()

    return IndexController;
});
