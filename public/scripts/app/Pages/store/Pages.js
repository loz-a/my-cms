define([
    'dojo/_base/declare',
    'dstore/Rest',
    'dstore/Cache',
    'dstore/Trackable',
    'PageModel'

], function(declare, Rest, Cache, Trackable, PageModel) { 'use strict';

    return declare([Rest, Cache, Trackable], {
        target: '/api/pages/',
        Model: PageModel
    });

});
