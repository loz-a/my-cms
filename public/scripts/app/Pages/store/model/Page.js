define([
    'dojo/_base/declare',
    'dmodel/Model',
    'dmodel/ComputedProperty'
], function(declare, Model, ComputedProperty) { "use strict";

    return declare(Model, {

        schema: {
            id: {
                type: 'number',
                required: false
            },
            alias: {
                type: 'string',
                required: true
            },
            slug: {
                type: 'string',
                required:true
            },
            title: {
                type: 'string',
                required: true
            },
            description: {
                type: 'string',
                required: false
            },
            author: {
                type: 'string',
                required: false
            },
            parsed_content: {
                type: 'string',
                required: true
            },
            preparsed_content: {
                type: 'string',
                required: true
            },
            created: {
                type: 'number',
                required: true
            },
            updated: {
                type: 'number',
                required: true
            },
            status: {
                type: 'string',
                required: true,
                "default": "draft"
            }
        }

    });

});