define({
    targetUrl: '/pages',

    'routes': {
        '/': {
            controller: 'Index',
            action: 'index'
        },
        '/create':  {
            controller: 'Index',
            action: 'create'
        },
        '/edit/:id': {
            controller: 'Index',
            action: 'edit'
        },
        '/delete/:id': {
            controller: 'Index',
            action: 'remove'
        }

    }
});