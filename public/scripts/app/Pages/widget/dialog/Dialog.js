define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/on',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'DialogTabsWidget',
    'DialogFormWidget',
    'dojo/text!./template/dialog.html'

], function(declare, lang, on, _WidgetBase, _TemplatedMixin, TabsWidget, FormWidget, template) {

    return declare([_WidgetBase, _TemplatedMixin], {

        baseClass: 'pages-dialog',

        templateString: template,

        tabsWidget: null,

        formWidget: null,

        show: function() {
            this.hideCreateButton();
            this.tabsWidget.showAttributesPane();
            this.domNode.style.display = '';
            return this;
        }, // show()


        hide: function() {
            this.showCreateButton();
            this.formWidget.reset();
            this.domNode.style.display = 'none';
            return this;
        }, // hide()


        hideCreateButton: function() {
            this.createButtonNode.style.display = 'none';
        }, // hideCreateButton()


        showCreateButton: function() {
            this.createButtonNode.style.display = '';
        }, // showCreateButton()


        postCreate: function() {
            this.tabsWidget = new TabsWidget({}, this.tabsNode);

            this.formWidget = new FormWidget({}, this.formNode);
            this.formWidget.setParentWidget(this);

            if (undefined === this.createButtonNode) {
                this.createButtonNode = document.querySelector('a[href="#/create"]');
            }
        }, // postCreate()


        startup: function() {
            this.inherited(arguments);
            this.tabsWidget.startup();
            this.formWidget.startup();
        } // startup()

    });

});