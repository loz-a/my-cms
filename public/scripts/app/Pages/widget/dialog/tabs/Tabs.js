define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dTBootstrap/Tabs',
    'dojo/text!./template/tabs.html'

], function(declare, _WidgetBase, _TemplatedMixin, Tabs, template) { 'use strict';

    if (!Element.prototype.closest) {
        Element.prototype.closest = function(selector) {
            var parent          = this.parentNode;
            var matchesSelector = this.matches || this.webkitMatchesSelector || this.mozMatchesSelector || this.msMatchesSelector;

            do {
                if (matchesSelector.bind(parent)(selector)) {
                    return parent;
                }
            } while(parent != win.body() && (parent = parent.parentNode) && parent.nodeType == 1);

            return null;
        };
    }

    return declare([_WidgetBase, _TemplatedMixin, Tabs], {

        baseClass: 'pages-dialog-tabs',

        templateString: template,

        showAttributesPane: function() {
            this.show(this.domNode.querySelector('a[href="#page-attributes"]'));
        }, // showAttributesPane()


        showContentPane: function() {
            this.show(this.domNode.querySelector('a[href="#page-content"]'));
        }, // showContentPane()


        showPaneByContainedNode: function(node) {
            var panelId = node.closest('.tab-pane').id;
            this.show(this.domNode.querySelector('a[href="#' + panelId + '"]'));
        } // showPaneByContainedNode()

    });
});