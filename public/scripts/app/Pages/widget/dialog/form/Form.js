define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/on',
    'put-selector/put',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dojo/text!./template/form.html'

], function(declare, lang, on, put, _WidgetBase, _TemplatedMixin, template) { 'use strict';

    if (!Element.prototype.closest) {
        Element.prototype.closest = function(selector) {
            var parent          = this.parentNode;
            var matchesSelector = this.matches || this.webkitMatchesSelector || this.mozMatchesSelector || this.msMatchesSelector;

            do {
                if (matchesSelector.bind(parent)(selector)) {
                    return parent;
                }
            } while(parent != win.body() && (parent = parent.parentNode) && parent.nodeType == 1);

            return null;
        };
    }

    return declare([_WidgetBase, _TemplatedMixin], {

        baseClass: 'pages-form',

        templateString: template,

        parentWidget: null,

        namesOfElements: ['id', 'alias', 'preparsed_content', 'status', 'title', 'description'],

        setParentWidget: function(parentWidget) {
            this.parentWidget = parentWidget;
            return this;
        }, // setParentWidget()


        getParentWidget: function() {
            return this.parentWidget;
        }, // getParentWidget()


        _setErrorsAttr: function(errors) {
            if (!lang.isObject(errors)) {
                throw new Error('Something wrong with the object that contains the errors');
            }

            var key, element, parent, elements = this.domNode.elements, firstErrorNode;

            for (key in errors) if (errors.hasOwnProperty(key)) {
                if (undefined !== errors[key]['messages']) {

                    if (undefined === elements[key]) {
                        throw new Error('Element "' + key + '" is missing in form elements');
                    }

                    element = elements[key];
                    parent = element.closest('.form-group');

                    if (!parent.classList.contains('has-error')) {
                        parent.classList.add('has-error');

                        this.errors[key] = {
                            messages:            errors[key]['messages'],
                            messageDOMContainer: put(element, "+span.help-block>span.fa.fa-exclamation-triangle< $", errors[key]['messages'][0]),
                            parentDOMContainer: parent
                        };


                    }

                    if (undefined === firstErrorNode) {
                        firstErrorNode = element;
                    }
                }
            }
            this.parentWidget.tabsWidget.showPaneByContainedNode(firstErrorNode);

            return this;
        }, // _setErrorsAttr()


        clearErrorMessage: function(errorKey) {
            var errors = this.errors, error;

            if (undefined === errors[errorKey]) return;
            error = errors[errorKey];

            error.messageDOMContainer.parentNode.removeChild(error.messageDOMContainer);
            error.parentDOMContainer.classList.remove('has-error');
            delete errors[errorKey];
        }, // clearErrorMessage()


        postCreate: function() {
            this.own(
                on(this.submitNode, 'click', this.submit.bind(this)),
                on(this.cancelNode, 'click', this.cancel.bind(this)),
                on(this.domNode, 'input:keyup, textarea:keyup', function(e) {
                    this.clearErrorMessage(e.target.name);
                }.bind(this))
            );

            this.errors = {};
        }, // postCreate()


        _setValuesAttr: function(data) {

            if (!lang.isObject(data)) {
                throw new Error('Something wrong with the object that contains the data');
            }

            var key, element, elements = this.domNode.elements, noe = this.namesOfElements;

            for (var i = 0; i < noe.length; ++i) {
                key = noe[i];
                if (undefined === elements[key]) {
                    throw new Error('Element "' + key + '" is missing in form elements');
                }

                element = elements[key];
                if (element.length) {
                    for (var j = 0; j < element.length; ++j) {
                        if (element[j].value === data.get(key)) {
                            element[j].checked = true;
                            break;
                        }
                    }
                } else {
                    element.value = data.get(key);
                }
            }

        }, // _setValuesAttr()


        _getValuesAttr: function() {
            var i, name, element, elements = this.domNode.elements, values = {};
            for (i = 0; i < this.namesOfElements.length; ++i) {
                name = this.namesOfElements[i];

                if (undefined === elements[name]) {
                    throw new Error('Element "' + name + '" is missing in form elements');
                }

                element = elements[name];
                if (element.length) {
                    for (var j = 0; j < element.length; ++j) {
                        if (element[j].checked) {
                            values[name] = element.value;
                        }
                    }
                } else {
                    values[name] = element.value;
                }
            }

            if (!values['id']) delete values['id'];

            return values;
        }, // _getValuesAttr()


        reset: function() {
            var key, errors = this.errors;

            for(key in errors) if (errors.hasOwnProperty(key)) {
                this.clearErrorMessage(key);
            }

            this.domNode.elements.id.value = '';
            this.domNode.reset();
        }, // reset()


        submit: function(e) {
           e.preventDefault();
           return this.onSubmit(this.get('values'));
        }, // submit()


        cancel: function(e) {
            return this.onCancel();
        },

        onSubmit: function() {}, // onSubmit()
        onCancel: function() {} // onCancel()

    });

});