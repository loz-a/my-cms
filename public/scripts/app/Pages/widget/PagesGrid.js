define([
    'dojo/_base/declare',
    'PagesStore',
    'dgrid/Grid',
    'dgrid/Keyboard',
    'dgrid/extensions/Pagination',
    'dgrid/Selector'
], function (declare, PagesStore, Grid, Keyboard, Pagination, Selector) { 'use strict';

    return declare([Grid, Keyboard, Pagination, Selector], {

        collection: new PagesStore(),

        rowsPerPage: 15,

        columns: {
            alias: {
                label: 'alias'
            },
            title: {
                label: 'title'
            },
            status: {
                label: 'status',
                sortable: false,
                get: function(object) {
                    return object.status === 1 ? 'draft' : 'public';
                }
            },
            actions: {
                label: 'actions',
                field: 'id',
                sortable: false,
                formatter: function(id) {
                    return '[<strong><i class="fa fa-pencil-square-o"></i></strong> <a href="#/edit/' + id + '"> edit</a>] [<strong><i class="fa fa-trash-o"></i></strong>  <a class="remove-row" href="#/delete/' + id + '"> delete</a>]';
                }
            }
        },

        show: function() {
            this.domNode.style.display = '';
            return this;
        }, // show()


        hide: function() {
            this.domNode.style.display = 'none';
            return this;
        } // hide()

    });

});