define([

    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/router'

], function(declare, lang, router) { "use strict";

    var App = declare(null, {

        config: {},
        router: null,
        controllers: {},

        constructor: function() {
            this.router = router;
        }, // constructor()


        configure: function(config) {
            if (lang.isObject(config)) this.config = config;
            return this;
        }, // configure()


        mixinControllers: function(controllers) {
            var name;
            for (name in controllers) if (controllers.hasOwnProperty(name)) {
                var controller = controllers[name];
                if (!lang.isObject(controller)) throw new Error('Wrong controller');
                if ('init' in controller && lang.isFunction(controller.init)) controller.init.call(controller, this);
                this.controllers[name] = controllers[name];
            }
            return this;
        }, // mixinController()


        registerRoutes: function() {
            if ('routes' in this.config && lang.isObject(this.config.routes)) {

                var name, routes = this.config.routes;
                for(name in routes) if (routes.hasOwnProperty(name)) {

                    var cbArgs = routes[name];
                    var callback = this.controllers[cbArgs['controller']][cbArgs['action']].bind(this.controllers[cbArgs['controller']]);

                    router.register(name, callback);
                }
                router.startup();
            }
        }, // registerRoutes()


        route: function(route, replace) {
            router.go(route, replace);
        } // route()

    });


    var __instance;
    App.getInstance = function(config) {
        if (__instance == undefined) {
            __instance = new App(config);
        }
        return __instance;
    }; // getInstance()

    return App;
});