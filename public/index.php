<?php
chdir(dirname(__DIR__));

require_once 'init_autoloader.php';
require_once 'Core/functions.php';

//$start = microtime(true);


Core\App::getInstance() -> init(include 'app/config.php') -> run();


//$end = microtime(true);
//$time = number_format(($end - $start), 2);
//echo 'This page loaded in ', $time, ' seconds';
?>

